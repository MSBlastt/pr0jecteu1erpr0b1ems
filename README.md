## What is this repository for? ##

Contains some selective solutions for Pr0ject Eu1er^ problems. 
The project is a website dedicated to a series of computational problems intended to be solved with computer programs, designed for people interested in mathematics and computer programming.

* Each solution generally has comments and basic explanation of used algorithm. 
* Requires JDK 8 to successfully compile and start each solution.
* Some solutions are plain and simple brute-force. Lame, I know. Eventually I may come back to it and redesign the algorithm.
* Each commit has approximate value of time taken for solution to be finished. Time is based on Intel i5 6900k CPU.


^ Project's name purposely ciphered in order to avoid search engine crawlers from indexing solutions. 
Since project itself discourage publishing solutions outside the project's internal forum.

## How do I get set up? ###

Check-out the code, run each solution, if needed.

## Contribution guidelines ##

No contribution expected, as this my personal collection. Mostly for self-development and JDK 8 features training (yes I'm still working with JDK 7 in 2017, ENTERPRISE). 
