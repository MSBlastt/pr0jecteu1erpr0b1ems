package projecteuler;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * Base class. Contains some useful common methods for each solution.
 *
 * Created by Mikhail Kholodkov
 *         on June 6, 2017.
 */
public abstract class ProjectEulerProblem {

    //Holds OS specific line break literal (/r/n, /n, etc)
    private static final String SYSTEM_LINE_BREAK = System.getProperty("line.separator");

    //Holds a timestamp of beginning of solution execution
    private static Date executionStart;

    static {
        executionStart = new Date();
        addShutdownTimePrintHook();
        disableBytecodeVerifier();
    }

    /**
     * Adds a hook which will be executed upon VM shutdown. Will print how much time solution had taken.
     * Helps to avoid copy-paste from problem to problem.
     */
    private static void addShutdownTimePrintHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(ProjectEulerProblem::calcAndPrintTimeTaken));
    }

    /**
     * Workaround to attach some profilers, such as VisualVM.
     * Incompatibility can be causes by some JDK/Profiler versions
     * <p>
     * Update: Seems like programmatically it's impossible to achieve.
     * Add -Xverify:none to JVM arguments in order to bypass bytecode verification
     */
    private static void disableBytecodeVerifier() {
        //impossible at runtime: https://stackoverflow.com/questions/43559229/set-noverify-flag-in-java-code
    }

    /**
     * Prints provided String.
     * Saves a bit of screen space: 'System.out.println'.length() > 'print'.length() !^_^!
     */
    protected static void print(Object s) {
        System.out.println(s);
    }

    /**
     * Prints provided String using String.format() + arguments.
     * Saves a bit of screen space: @see print()
     */
    protected static void print(String s, Object... arguments) {
        System.out.println(String.format(s, arguments));
    }

    /**
     * Prints an empty line (two line breaks) in console. Platform independent.
     */
    protected static void printEmptyLine() {
        System.out.println(SYSTEM_LINE_BREAK);
    }

    /**
     * Prints a line break in console. Platform independent.
     */
    protected static void lineBreak() {
        System.out.print(SYSTEM_LINE_BREAK);
    }

    /**
     * Does exactly what it says. Hook will call this method before JVM will end its lifecycle.
     */
    protected static void calcAndPrintTimeTaken() {
        lineBreak();
        Long timeTaken = calcAndGetTimeTakenValue();
        print("Time taken: %s ms (~%s sec)", timeTaken, new DecimalFormat("#.#").format(Double.valueOf(timeTaken / 1_000.00)));
    }

    private static String calcAndGetTimeTaken() {
        return calcAndGetTimeTakenValue() + " ms";
    }

    private static Long calcAndGetTimeTakenValue() {
        return new Date().getTime() - executionStart.getTime();
    }

    /**
     * Prints given solution on a new line.
     *
     * Overloaded method additionally forces JVM to quit.
     * Can be useful when solution is found and further execution is senseless,
     * but cannot be stopped programmatically (i.e. inside a stream pipeline).
     */
    protected static void printSolution(Object solution) {
        System.out.println(System.getProperty("line.separator") +
                "Solution is: " + solution);
    }

    protected static void printSolutionAndExit(Object solution) {
        printSolution(solution.toString());
        System.exit(0);
    }
}
