package projecteuler.helper;

import java.util.Arrays;

/**
 * Modified and improved version of BigInteger.java class
 * 
 * Most of the default methods have significant performance improvements,
 * such as add(), pow(), divide(), etc...
 * 
 * Some methods have been removed from the original for simplicity and compactness,
 * maybe added in the future, if needed.
 *
 * Source: https://github.com/bwakell/Huldra/blob/master/src/main/java/org/huldra/math/BigInt.java
 * @author Simon Klein
 * @version 0.7
 *
 * Extended with toStringLastNine() method which returns only last 9 characters from a string representation of a value.
 * 
 * Created by Mikhail Kholodkov
 *         on 9/12/2017.
 */
public class BigInt extends Number implements Comparable<BigInt> {
    /**
     * Used to cast a (base 2^32) digit to a long without getting unwanted sign extension.
     */
    private static final long mask = (1L << 32) - 1;

    /**
     * The sign of this number.
     * 1 for positive numbers and -1 for negative numbers.
     * Zero can have either sign.
     */
    private int sign;
    /**
     * The number of digits of the number (in base 2^32).
     */
    private int len;
    /**
     * The digits of the number, i.e., the magnitude array.
     */
    private int[] dig;


    /**
     * Creates a BigInt from the given int.
     * The input-value will be interpreted a signed value.
     *
     * @param val The value of the number.
     * @complexity O(1)
     */
    public BigInt(final int val) {
        dig = new int[1];
        assign(val);
    }

    /**
     * Reallocates the magnitude array to one twice its size.
     *
     * @complexity O(n)
     */
    private void realloc() {
        final int[] res = new int[dig.length * 2];
        System.arraycopy(dig, 0, res, 0, len);
        dig = res;
    }

    /**
     * Reallocates the magnitude array to one of the given size.
     *
     * @param newLen The new size of the magnitude array.
     * @complexity O(n)
     */
    private void realloc(final int newLen) {
        final int[] res = new int[newLen];
        System.arraycopy(dig, 0, res, 0, len);
        dig = res;
    }

    /**
     * Assigns the given number to this BigInt object.
     *
     * @param s   The sign of the number.
     * @param val The magnitude of the number (will be intepreted as unsigned).
     * @complexity O(1)
     */
    public void uassign(final int s, final int val) {
        sign = s;
        len = 1;
        dig[0] = val;
    }


    /**
     * Assigns the given number to this BigInt object.
     *
     * @param val The number to be assigned.
     * @complexity O(1)
     */
    public void assign(final int val) {
        uassign(val < 0 ? -1 : 1, val < 0 ? -val : val);
    }

    /**
     * Tells whether this number is zero or not.
     *
     * @return true if this number is zero, false otherwise
     * @complexity O(1)
     */
    public boolean isZero() {
        return len == 1 && dig[0] == 0;
    }

    /**
     * Compares the absolute value of this and the given number.
     *
     * @param a The number to be compared with.
     * @return -1 if the absolute value of this number is less, 0 if it's equal, 1 if it's greater.
     * @complexity O(n)
     */
    public int compareAbsTo(final BigInt a) {
        if (len > a.len) return 1;
        if (len < a.len) return -1;
        for (int i = len - 1; i >= 0; i--)
            if (dig[i] != a.dig[i])
                if ((dig[i] & mask) > (a.dig[i] & mask)) return 1;
                else return -1;
        return 0;
    }

    /**
     * Compares the value of this and the given number.
     *
     * @param a The number to be compared with.
     * @return -1 if the value of this number is less, 0 if it's equal, 1 if it's greater.
     * @complexity O(n)
     */
    public int compareTo(final BigInt a) {
        if (sign < 0) {
            if (a.sign < 0 || a.isZero()) return -compareAbsTo(a);
            return -1;
        }
        if (a.sign > 0 || a.isZero()) return compareAbsTo(a);
        return 1;
    }

    /**
     * Tests equality of this number and the given one.
     *
     * @param a The number to be compared with.
     * @return true if the two numbers are equal, false otherwise.
     * @complexity O(n)
     */
    public boolean equals(final BigInt a) {
        if (len != a.len) return false;
        if (isZero() && a.isZero()) return true;
        if ((sign ^ a.sign) < 0) return false; //In case definition of sign would change...
        for (int i = 0; i < len; i++) if (dig[i] != a.dig[i]) return false;
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) //Todo: Equality on other Number objects?
    {
        if (o instanceof BigInt) return equals((BigInt) o);
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 0; //Todo: Opt and improve.
        for (int i = 0; i < len; i++) hash = (int) (31 * hash + (dig[i] & mask));
        return sign * hash; //relies on 0 --> hash==0.
    }
    /*** </General functions> ***/

    /*** <Number Override> ***/
    /**
     * {@inheritDoc}
     * Returns this BigInt as a {@code byte}.
     *
     * @return {@code sign * (this & 0x7F)}
     */
    @Override
    public byte byteValue() {
        return (byte) (sign * (dig[0] & 0x7F));
    }

    /**
     * {@inheritDoc}
     * Returns this BigInt as a {@code short}.
     *
     * @return {@code sign * (this & 0x7FFF)}
     */
    @Override
    public short shortValue() {
        return (short) (sign * (dig[0] & 0x7FFF));
    }

    /**
     * {@inheritDoc}
     * Returns this BigInt as an {@code int}.
     *
     * @return {@code sign * (this & 0x7FFFFFFF)}
     */
    @Override
    public int intValue() {
        return sign * (dig[0] & 0x7FFFFFFF); //relies on that sign always is either 1/-1.
    }

    /**
     * {@inheritDoc}
     * Returns this BigInt as a {@code long}.
     *
     * @return {@code sign * (this & 0x7FFFFFFFFFFFFFFF)}
     */
    @Override
    public long longValue() {
        return len == 1 ? sign * (dig[0] & mask) : sign * ((dig[1] & 0x7FFFFFFFL) << 32 | (dig[0] & mask));
    }

    /**
     * {@inheritDoc}
     * Returns this BigInt as a {@code float}.
     *
     * @return the most significant 24 bits in the mantissa (the highest order bit obviously being implicit),
     * the exponent value which will be consistent for {@code BigInt}s up to 128 bits (should it not fit it'll be calculated modulo 256),
     * and the sign bit set if this number is negative.
     */
    @Override
    public float floatValue() {
        final int s = Integer.numberOfLeadingZeros(dig[len - 1]);
        if (len == 1 && s >= 8) return sign * dig[0];

        int bits = dig[len - 1]; //Mask out the 24 MSBits.
        if (s <= 8) bits >>>= 8 - s;
        else bits = bits << s - 8 | dig[len - 2] >>> 32 - (s - 8); //s-8==additional bits we need.
        bits ^= 1L << 23; //The leading bit is implicit, cancel it out.

        final int exp = (int) (((32 - s + 32L * (len - 1)) - 1 + 127) & 0xFF);
        bits |= exp << 23; //Add exponent.
        bits |= sign & (1 << 31); //Add sign-bit.

        return Float.intBitsToFloat(bits);
    }

    /**
     * {@inheritDoc}
     * Returns this BigInt as a {@code double}.
     *
     * @return the most significant 53 bits in the mantissa (the highest order bit obviously being implicit),
     * the exponent value which will be consistent for {@code BigInt}s up to 1024 bits (should it not fit it'll be calculated modulo 2048),
     * and the sign bit set if this number is negative.
     */
    @Override
    public double doubleValue() {
        if (len == 1) return sign * (dig[0] & mask);

        final int s = Integer.numberOfLeadingZeros(dig[len - 1]);
        if (len == 2 && 32 - s + 32 <= 53) return sign * ((long) dig[1] << 32 | (dig[0] & mask));

        long bits = (long) dig[len - 1] << 32 | (dig[len - 2] & mask); //Mask out the 53 MSBits.
        if (s <= 11) bits >>>= 11 - s;
        else bits = bits << s - 11 | dig[len - 3] >>> 32 - (s - 11); //s-11==additional bits we need.
        bits ^= 1L << 52; //The leading bit is implicit, cancel it out.

        final long exp = ((32 - s + 32L * (len - 1)) - 1 + 1023) & 0x7FF;
        bits |= exp << 52; //Add exponent.
        bits |= (long) sign & (1L << 63); //Add sign-bit.

        return Double.longBitsToDouble(bits);
    }


    /*** </Signed Small Num> ***/

    /*** <Big Num Helper> ***/
    /**
     * Increases the magnitude of this number by the given magnitude array.
     *
     * @param v    The magnitude array of the increase.
     * @param vlen The length (number of digits) of the increase.
     * @complexity O(n)
     */
    private void addMag(int[] v, int vlen) {
        int ulen = len;
        int[] u = dig; //ulen <= vlen
        if (vlen < ulen) {
            u = v;
            v = dig;
            ulen = vlen;
            vlen = len;
        }
        if (vlen > dig.length) realloc(vlen + 1);

        long carry = 0;
        int i = 0;
        for (; i < ulen; i++) {
            carry = (u[i] & mask) + (v[i] & mask) + carry;
            dig[i] = (int) carry;
            carry >>>= 32;
        }
        if (vlen > len) {
            System.arraycopy(v, len, dig, len, vlen - len);
            len = vlen;
        }
        if (carry != 0) //carry==1
        {
            for (; i < len && ++dig[i] == 0; i++) ;
            if (i == len) //vlen==len
            {
                if (len == dig.length) realloc();
                dig[len++] = 1;
            }
        }
    }

    /**
     * Decreases the magnitude of this number by the given magnitude array.
     * Behaviour is undefined if u > |this|.
     *
     * @param u The magnitude array of the decrease.
     * @complexity O(n)
     */
    private void subMag(final int[] u, final int ulen) {
        final int vlen = len;
        final int[] v = dig; //ulen <= vlen

        //Assumes vlen=len and v=dig
        long dif = 0;
        int i = 0;
        for (; i < ulen; i++) {
            dif = (v[i] & mask) - (u[i] & mask) + dif;
            dig[i] = (int) dif;
            dif >>= 32;
        }
        if (dif != 0) {
            for (; dig[i] == 0; i++) --dig[i];
            if (--dig[i] == 0 && i + 1 == len) len = ulen;
        }
        while (len > 1 && dig[len - 1] == 0) --len;
    }
    /*** </Big Num Helper> ***/

    /*** <Big Num> ***/
    /**
     * Adds a BigInt to this number.
     *
     * @param a The number to add.
     * @complexity O(n)
     */
    public void add(final BigInt a) {
        if (sign == a.sign) {
            addMag(a.dig, a.len);
            return;
        }
        if (compareAbsTo(a) >= 0) {
            subMag(a.dig, a.len);
            //if(len==1 && dig[0]==0) sign = 1;
            return;
        }

        final int[] v = a.dig;
        final int vlen = a.len;
        if (dig.length < vlen) realloc(vlen + 1);

        sign = -sign;
        long dif = 0;
        int i = 0;
        for (; i < len; i++) {
            dif = (v[i] & mask) - (dig[i] & mask) + dif;
            dig[i] = (int) dif;
            dif >>= 32;
        }
        if (vlen > len) {
            System.arraycopy(v, len, dig, len, vlen - len);
            len = vlen;
        }
        if (dif != 0) {
            for (; i < vlen && dig[i] == 0; i++) --dig[i];
            if (--dig[i] == 0 && i + 1 == len) --len;
        }
        //if(i==vlen) should be impossible
    }
    /*** </Big Num> ***/

    /*** <Output> ***/
    /**
     * Converts this number into a string of radix 10.
     *
     * @return The string representation of this number in decimal.
     * @complexity O(n^2)
     */
    public String toString() {
        if (isZero()) return "0";

        int top = len * 10 + 1;
        final char[] buf = new char[top];
        Arrays.fill(buf, '0');
        final int[] cpy = Arrays.copyOf(dig, len);
        while (true) {
            final int j = top;
            for (long tmp = toStringDiv(); tmp > 0; tmp /= 10)
                buf[--top] += tmp % 10; //TODO: Optimize.
            if (len == 1 && dig[0] == 0) break;
            else top = j - 13;
        }
        if (sign < 0) buf[--top] = '-';
        System.arraycopy(cpy, 0, dig, 0, len = cpy.length);
        return new String(buf, top, buf.length - top);
    }

    /**
     * Returns last 9 digits of string representation of a big integer.
     * Complexity is constant.
     */
    public String toStringLastNine() {
        if (isZero()) {
            return "0";
        }

        final int[] cpy = Arrays.copyOf(dig, len);

        long l = toStringDiv();

        System.arraycopy(cpy, 0, dig, 0, len = cpy.length);

        return String.valueOf(l).substring(4);
    }

    // Divides the number by 10^13 and returns the remainder.
    // Does not change the sign of the number.
    private long toStringDiv() {
        final int pow5 = 1_220_703_125, pow2 = 1 << 13;
        int nextq = 0;
        long rem = 0;
        for (int i = len - 1; i > 0; i--) {
            rem = (rem << 32) + (dig[i] & mask);
            final int q = (int) (rem / pow5);
            rem = rem % pow5;
            dig[i] = nextq | q >>> 13;
            nextq = q << 32 - 13;
        }
        rem = (rem << 32) + (dig[0] & mask);
        final int mod2 = dig[0] & pow2 - 1;
        dig[0] = nextq | (int) (rem / pow5 >>> 13);
        rem = rem % pow5;
        // Applies the Chinese Remainder Theorem.
        // -67*5^13 + 9983778*2^13 = 1
        final long pow10 = (long) pow5 * pow2;
        rem = (rem - pow5 * (mod2 - rem) % pow10 * 67) % pow10;
        if (rem < 0) rem += pow10;
        if (dig[len - 1] == 0 && len > 1)
            if (dig[--len - 1] == 0 && len > 1)
                --len;
        return rem;
    }
}
