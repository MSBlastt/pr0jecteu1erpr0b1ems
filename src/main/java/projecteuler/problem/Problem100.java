package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

import java.math.BigDecimal;

/**
 * Problem 100: Arranged probability
 *
 * If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs,
 * and two discs were taken at random, it can be seen that the probability of taking two blue discs,
 * P(BB) = (15/21)×(14/20) = 1/2.
 *
 * The next such arrangement, for which there is exactly 50% chance of taking two blue discs at random,
 * is a box containing eighty-five blue discs and thirty-five red discs.
 *
 * By finding the first arrangement to contain over 1012 = 1,000,000,000,000 discs in total,
 * determine the number of blue discs that the box would contain.
 *
 * Created by Mikhail Kholodkov
 *         on July 22, 2017.
 */
public class Problem100 extends ProjectEulerProblem {

    public static void main(String[] args) {
        //Defining goals
        double goal = 1_000_000_000_000L, probabilityGoal = 0.5;

        /* Coefficient is a ratio between amount of blue and total discs.
           Multiplication Coefficient is a ratio between latest found amount of blue discs and current pair.
           These values grow (getting more precise) after each new found pair.
           Therefore needs to be updated ever after.
           Preciseness of these values is critical. It drives the whole solution in less than a second.
           Otherwise, brute-forcing every solution will take forever.
           There's probably a better formula can be found to describe this.
         */
        BigDecimal coefficient = BigDecimal.valueOf(15d / 21d), multiplicationCoefficient;

        //Given in a description
        long latestFoundBlueDiscsCount = 15L, discsTotal = 21L;

        //Iterating total amount of discs until goal is reached
        boolean goalReached = false;
        while (!goalReached) {
            long count = (long) (discsTotal * coefficient.doubleValue()); //blue discs count
            boolean foundMatch = false;

            //Iterating amount of blue discs until pair is found. Blue discs count = totalDiscs * coefficient (~0.707)
            while (!foundMatch) {

                double probability = countProbability(count, discsTotal); //getting probability, 0.5 = 50%

                //If probability getting higher than 50% (i.e. 0.5000000...0001), then breaking inner loop
                if (probability > probabilityGoal) {
                    break;
                }

                //if probability matches, printing pair, updating coefficients and other metadata.
                if (foundMatch = probability == probabilityGoal) {
                    print("Found match: " + count + " blue discs, for total amount of discs: " + discsTotal);

                    goalReached = discsTotal > goal;

                    if (!goalReached) {
                        coefficient = getDivision(count, discsTotal);
                        multiplicationCoefficient = getDivision(count, latestFoundBlueDiscsCount);

                        discsTotal = (long) (discsTotal * multiplicationCoefficient.doubleValue());
                    }

                    latestFoundBlueDiscsCount = count;
                }

                count++;
            }

            discsTotal++;
        }

        printSolution(latestFoundBlueDiscsCount);
    }

    /**
     * Gets probability P(BB) for 2 events, decrementing the total amount of possibilities.
     *
     * todo: Needs to be generified and moved to Utils
     */
    private static double countProbability(long count, long amount) {
        return getDivision(count, amount).multiply(getDivision(--count, --amount)).doubleValue();
    }

    /**
     * Returns count / amount as a BigDecimal, with 100 mantissa scale. Rounding down.
     */
    private static BigDecimal getDivision(long count, long amount) {
        return BigDecimal.valueOf(count).divide(BigDecimal.valueOf(amount), 100, BigDecimal.ROUND_FLOOR);
    }

}