package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.helper.BigInt;
import projecteuler.util.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Problem 104: Pandigital Fibonacci ends
 *
 *
 * The Fibonacci sequence is defined by the recurrence relation:
 * 
 * Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
 * It turns out that F541, which contains 113 digits, is the first Fibonacci number for which the last nine digits are 1-9 pandigital (contain all the digits 1 to 9, but not necessarily in order).
 * And F2749, which contains 575 digits, is the first Fibonacci number for which the first nine digits are 1-9 pandigital.
 * 
 * Given that Fk is the first Fibonacci number for which the first nine digits AND the last nine digits are 1-9 pandigital, find k.
 * 
 * Created by Mikhail Kholodkov
 *         on July 25, 2017.
 */
public class Problem104 extends ProjectEulerProblem {

    //List of fibonacci sequence started from first two values. Will be updated each time when next member will be found
    private static List<BigInt> fibonacciList = new ArrayList<>(Arrays.asList(new BigInt(1), new BigInt(1)));

    public static void main(String[] args) {

        //Looping infinitely until solution is found. Starting from 3rd element in a sequence.
        for (int i = 3; ; i++) {

            BigInt fibonacci = getFibonacci();

            //Skipping first 40 members of chain, since F(39) has less than 9 digits overall, thus cannot be a solution
            if (i >= 40) {

                //Extracting last 9 digits as a string, from a monstrous number, wrapped in a helper BigInt class
                String fibonacciString = fibonacci.toStringLastNine();

                //If string contains zero it doesn't match solution (1-9 pandigital), if it is comparing first 9 chars
                if (!fibonacciString.contains("0") && Utils.isPandigital(Integer.valueOf(fibonacciString))) {

                    print("Found fibonacci sequence member with last 9 digits being a pandigital number, I = " + i);

                    //Since last 9 digits satisfies requirement, if first 9 digits satisfies too, then it is a solution
                    if (Utils.isPandigital(Integer.valueOf(fibonacci.toString().substring(0, 9)))) {
                        printSolution(i);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Calculating next fibonacci sequence number based on last two values in the list.
     * Once value is calculated it will be added to the list for future use.
     *
     * Uses helper BigInt class. Without it add() method will be drastically slower for these kind of big numbers.
     */
    private static BigInt getFibonacci() {
        BigInt bigInt = fibonacciList.get(fibonacciList.size() - 2);

        bigInt.add(fibonacciList.get(fibonacciList.size() - 1)); //Modified class returns void, and stores value inside

        fibonacciList.add(bigInt);

        return bigInt;
    }
}