package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.Arrays;
import java.util.List;

/**
 * Problem 108: Diophantine reciprocals I
 *
 * In the following equation x, y, and n are positive integers.
 * 1/x + 1/y = 1/n
 *
 * For n = 4 there are exactly three distinct solutions:
 * 1/5 + 1/20 = 1/4
 * 1/6 + 1/12 = 1/4
 * 1/8 + 1/8 = 1/4
 *
 * What is the least value of n for which the number of distinct solutions exceeds one-thousand?
 *
 * NOTE: This problem is an easier version of Problem 110; it is strongly advised that you solve this one first.
 *
 * Created by Mikhail Kholodkov
 *         on July 29, 2017.
 */
public class Problem108 extends ProjectEulerProblem {

    public static void main(String[] args) {
        int highestCount = 0, highestN;

        List<Integer> highlyAbundantNumbers =
                Arrays.asList(1, 2, 3, 4, 6, 8, 10, 12, 16, 18, 20, 24, 30, 36, 60, 72, 84, 90, 96, 108, 120, 144);

        /* Let's take the formula: 1/x + 1/y = 1/n ==> (X-N)*(Y-N) = N^2
           I can assume (well not me, Wikipedia does) that there are as many solutions
           of (x,y) as there are divisors of N^2 less than or equal to N.
           So let's iterate N and we'll be looking for divisors count > 1000
           Even numbers has more or same amount of divisors as closest odd number (i.e. 75/76).
           Moreover we'll be iterating with a step of a highly abundant number [1],
           since such numbers are usually highly more divisible than closest (or any lowest) numbers.
           This assumption may be wrong, but doubtful. If solution won't be found close to a goal,
           then we'll pick next highly abundant number from a list, going from highest to lowest, for precision.

           [1] - https://en.wikipedia.org/wiki/Highly_abundant_number
         */
        int step = highlyAbundantNumbers.get(highlyAbundantNumbers.size() - 5); //adjust element index for accuracy
        for (int n = step;; n += step) {

            //Getting divisors count for N^2, where divisor should be less or equal to N
            int divisorsCount = Utils.getDivisors((long) Math.pow(n, 2), n).size();

            if (highestCount < divisorsCount) {
                highestCount = divisorsCount;
                highestN = n;

                print("Found new high: " + highestCount + ", for N=" + n);

                if (highestCount > 1000) {
                    break;
                }
            }
        }

        printSolution(highestN);
    }
}