package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

/**
 * Problem 108: Diophantine reciprocals II
 *
 * In the following equation x, y, and n are positive integers.
 * 1/x + 1/y = 1/n
 *
 * It can be verified that when n = 1260 there are 113 distinct solutions
 * and this is the least value of n for which the total number of distinct solutions exceeds one hundred.
 *
 * What is the least value of n for which the number of distinct solutions exceeds four million?
 *
 * NOTE: This problem is a much more difficult version of Problem 108
 * and as it is well beyond the limitations of a brute force approach it requires a clever implementation.
 *
 * Created by Mikhail Kholodkov
 *         on July 31, 2017.
 */
public class Problem110 extends ProjectEulerProblem {

    public static void main(String[] args) {
        /*
          As it was realized during solving problem #108, that
          Number of distinct solutions is equal to amount of divisors(N^2), where each divisor <= N.

          Assuming that the amount of divisors(N^2) are combinations which can be made from its prime factorization [1],
          and if that number can be expressed as p1^a * p2^b * p3^c, then it has (a+1)*(b+1)*(c+1)*(...) divisors [2].

          Since N^2 can't have odd amount of the same prime in it's decomposition [3],
          we can calculate maximum amount of primes as log(4000000*2-2)/log(3)=14.46.

          Considering that we need to minimize the number, we must use only smallest possible primes.
          Thus the only the first 15 primes will be relevant.

          From this point we can make a recursive method which checks every possible relevant number (~500k),
          and returns the minimum of those that have more than 4000000*2-2 divisors.

          [1] - https://en.wikipedia.org/wiki/Prime_factor
          [2] - https://math.stackexchange.com/questions/433848/prime-factors-number-of-divisors
          [3] - https://en.wikipedia.org/wiki/Integer_factorization#Prime_decomposition
        */
        long goal = 4000000 * 2 - 2;

        long[] primes = Utils.getFirstNPrimes(15)
                .stream()
                .mapToLong(i -> i)
                .toArray();


        recursivePrimeFactorization(goal, primes, 0, 1, 1);

        printSolution(solutionN);
    }

    //Value will be decremented for each new found solution, until it goes beyond goal and when it does - solution found
    private static long solutionN = Long.MAX_VALUE;

    /**
     * Modified version of a simple Prime factors search using recursion method.
     *
     * Algorithm description can be found here: https://en.wikipedia.org/wiki/Integer_factorization
     */
    private static void recursivePrimeFactorization(long goal, long[] primes, int pos, long num, int amount) {
        if (amount >= goal) {
            solutionN = num;
            return;
        }

        long nextNum, nextAmount = amount, primeAmount = 0;

        while (nextAmount <= goal) {
            primeAmount += 1;
            nextNum = num * (long) Math.pow(primes[pos], (int) primeAmount);
            nextAmount = amount * (primeAmount * 2 + 1);

            if (nextNum > solutionN || nextNum <= 0 ||
                    nextAmount > 5 * ((long) Math.pow(10, 7)) || nextAmount <= 0) {
                return;
            }

            recursivePrimeFactorization(goal, primes, pos + 1, nextNum, (int) nextAmount);
        }
    }
}