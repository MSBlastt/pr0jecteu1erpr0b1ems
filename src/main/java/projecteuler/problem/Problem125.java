package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Problem 125: Palindromic sums
 *
 * The palindromic number 595 is interesting because it can be written as the sum of consecutive squares:
 * 6^2 + 7^2 + 8^2 + 9^2 + 10^2 + 11^2 + 12^2.
 *
 * There are exactly eleven palindromes below one-thousand that can be written as consecutive square sums,
 * and the sum of these palindromes is 4164.
 *
 * Note that 1 = 0^2 + 1^2 has not been included as this problem is concerned with the squares of positive integers.
 *
 * Find the sum of all the numbers less than 10^8 that are both palindromic and can be written as the sum of consecutive squares.
 *
 * Created by Mikhail Kholodkov
 *         on September 10, 2017.
 */
public class Problem125 extends ProjectEulerProblem {

    private static final int LIMIT = 100_000_000; //Goal limit

    public static void main(String[] args) {
        List<Integer> palindromes = new ArrayList<>();
        List<Integer> squares = new ArrayList<>();

        //Iterating from 1 to LIMIT (one hundred million), adding found palindromes to list and squares below limit
        IntStream.range(1, LIMIT)
                .parallel()
                .filter(Utils::isPalindrome)
                .forEachOrdered(palindromes::add);

        //Iteration stops when next two squares sum will be more than the LIMIT (x ^ 2 + x ^ 2 > 100_000_000)
        IntStream.range(1, (int) Math.sqrt(LIMIT / 2)) //[1...~7100]
                .parallel()
                .forEachOrdered(i -> squares.add(i * i));

        //Printing amount of found palindromes and squares
        print("Found %s palindromes", palindromes.size());
        print("Added %s squares", squares.size());

        List<Integer> resultList = new ArrayList<>();

        //Iterating over list of found palindromes
        for (Integer palindrome : palindromes) {


            /* Iterating over squares map, summarizing each next value.
             *
             * If sum is more than original palindrome, then tempSum will subtract values starting from the beginning.
             * Once tempSum is less than Palindrome again, continuing array's iteration.
             *
             * Algorithm is very efficient O (n). Works great!
             * Thanks to interviewers at "Informzashchita", Moscow. They showed me this just recently :)
             *
             * Minimum two values needs to be summarized. 1 = 1 ^ 2 doesn't count.
             *
             */
            int tempSum = 0, tempIndex = 0;
            for (Integer square : squares) {

                tempSum += square;

                while (tempSum > palindrome) {
                    tempSum -= squares.get(tempIndex);
                    tempIndex++;
                }

                //true, if palindrome and sum of consecutive squares (minimum 2) equal
                if (tempSum == palindrome && !square.equals(palindrome)) {
                    resultList.add(palindrome);
                    break;
                }
            }
        }

        //Intermediate print of found palindromes and their count for which consecutive sum of squares exist
        lineBreak();
        print(resultList);
        print("Total solutions found %s", resultList.size());

        printSolution(resultList.stream().mapToLong(i -> i).sum());
    }
}