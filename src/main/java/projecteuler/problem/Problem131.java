package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Problem 125: Palindromic sums
 *
 * There are some prime values, p, for which there exists a positive integer, n,
 * such that the expression n^3 + n^2*p is a perfect cube.
 *
 * For example, when p = 19, 8^3 + 8^2×19 = 123.
 *
 * What is perhaps most surprising is that for each prime with this property the value of n is unique,
 * and there are only four such primes below one-hundred.
 *
 * How many primes below one million have this remarkable property?
 *
 * Created by Mikhail Kholodkov
 *         on September 22, 2017.
 */
public class Problem131 extends ProjectEulerProblem {

    private static final int PRIME_MAX_VALUE = 1_000_000;

    public static void main(String[] args) {
        //Final count
        int result = 0;

        /* Iterating from 1 until desired value (prime or not) will be more than a limit (1 mil)
           During development of this algorithm it was clear that each found solution has a cube itself,
           as a base N value:

               Formula: 1^3 + 1^2 * 7 = 2 ^ 3. Result is 8
               Formula: 8^3 + 8^2 * 19 = 12 ^ 3. Result is 1728
               Formula: 27^3 + 27^2 * 37 = 36 ^ 3. Result is 46656
               Formula: 64^3 + 64^2 * 61 = 80 ^ 3. Result is 512000

           Therefore it's enough to iterate only over cubes, then by simplifying formula we can find which value as P
           will make a perfect cube. Once value is calculated, we simply check if it's prime or not.

           Solution is instant.
         */

        for (int i = 1; ; i++) {

            //baseCube is the base value which will be used for future calculation
            BigInteger bigIntegerI = BigInteger.valueOf(i);
            BigInteger baseCube = bigIntegerI.multiply(bigIntegerI).multiply(bigIntegerI);

            BigInteger square = baseCube.multiply(baseCube);
            BigInteger cube = square.multiply(baseCube);

            double iRoot = Math.cbrt(baseCube.doubleValue());
            BigInteger iRootBigInteger = new BigDecimal(iRoot).toBigInteger();

            //Right part of formula until powered to cube
            BigInteger rightSideToBeCube = iRootBigInteger.multiply(iRootBigInteger).add(baseCube);

            //Right part of formula
            BigInteger rightSideCube = rightSideToBeCube.multiply(rightSideToBeCube).multiply(rightSideToBeCube);

            //Simplified formula (N + cbrt(N) * cbrt(N) - N^3 / N^2;) and desired value for it.
            BigInteger expectedPrime = rightSideCube.subtract(cube).divide(square);

            int wannabePrime = expectedPrime.intValue(); //If value is a prime - we found next solution!
            if (Utils.isPrime(wannabePrime)) {
                print("Found perfect cube/prime relationship. Formula: %s^3 + %s^2 * %s = %s ^ 3. Result is %s",
                        baseCube, baseCube, wannabePrime, rightSideToBeCube, rightSideCube);

                result++;
            }

            if (wannabePrime > PRIME_MAX_VALUE) {
                break;
            }
        }

        printSolution(result);
    }
}