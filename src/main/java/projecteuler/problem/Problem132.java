package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Problem 132: Large repunit factors
 * <p>
 * A number consisting entirely of ones is called a repunit. We shall define R(k) to be a repunit of length k.
 * <p>
 * For example, R(10) = 1111111111 = 11×41×271×9091, and the sum of these prime factors is 9414.
 * <p>
 * Find the sum of the first forty prime factors of R(10^9).
 * <p>
 * Created by Mikhail Kholodkov
 * on September 24, 2017.
 */
public class Problem132 extends ProjectEulerProblem {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        List<Long> result = new ArrayList<>(); //final list of prime multipliers

        List<Long> primes = Utils.getPrimesArray(2L, 200000L);// up to 200k should be sufficiently large for first 40 primes

        BigInteger k = BigInteger.valueOf((int) Math.pow(10, 9)); //Constant K digit repunit (10^9)

        /* By investigating repunits, we can realize (or find it on wikipedia),
           that we can represent a k digit repunit as:

           R(k) = 10^k - 1 / 9

           Taking this into account.
           In order to check if a prime number P is a prime factor, we can check if the modulo is 0.
           So for a repunit we can check:

           10^k - 1 / 9  ≡ 0 (mod P)  ==>  10^k - 1 ≡ 0 (mod 9P)  ==>  10^k ≡ 1 (mod 9P)

           By considering this, the function seems pretty easy, despite very large final number.
           Java modPow() method makes it very easy, even to brute-force through all available primes.

           Solution is instant.
         */
        for (int i = 0; result.size() < 40; i++) {

            if (new BigInteger(String.valueOf(10))
                    .modPow(k, BigInteger.valueOf(9 * primes.get(i))).equals(BigInteger.ONE)) {
                result.add(primes.get(i));
            }
        }

        printSolution(result.stream().mapToLong(l -> l).sum());
    }
}