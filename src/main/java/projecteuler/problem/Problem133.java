package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigInteger;
import java.util.*;

/**
 * Problem 133: Repunit nonfactors
 *
 * A number consisting entirely of ones is called a repunit. We shall define R(k) to be a repunit of length k;
 * for example, R(6) = 111111.
 *
 * Let us consider repunits of the form R(10^n).
 *
 * Although R(10), R(100), or R(1000) are not divisible by 17, R(10000) is divisible by 17.
 * Yet there is no value of n for which R(10^n) will divide by 19. In fact, it is remarkable that 11, 17, 41, and 73
 * are the only four primes below one-hundred that can be a factor of R(10^n).
 *
 * Find the sum of all the primes below one-hundred thousand that will never be a factor of R(10^n).
 *
 * Created by Mikhail Kholodkov
 *         on September 26, 2017.
 */
public class Problem133 extends ProjectEulerProblem {

    private static final long PRIMES_LIMIT = 100_000L;

    public static void main(String[] args) {
        Long result = 0L; //Final result

        List<Long> primes = Utils.getPrimesArray(2L, PRIMES_LIMIT); //Getting array of primes between 2 and 100_000

        BigInteger base = BigInteger.valueOf(10);
        BigInteger k = base.pow(1000); //Upper-bound of N is more like a random guess. 10^1000 should be sufficient though.

        /* For more details:
           @see Problem132.java

           The real difference here is that we need to filter all prime factors for every R(10^N).
           Rest of primes below one-hundred thousand is solution.

           This method implies that N should be taken big enough to cover all possible factors.
           Thus it's a sort of guess for upper-bound N. It's not a perfect algorithm, but works.

           Solution is instant.
         */
            for (Long prime : primes) {

                if (!base.modPow(k, BigInteger.valueOf(9 * prime)).equals(BigInteger.ONE)) {
                    result += prime;
                }
            }

        printSolution(result);
    }
}