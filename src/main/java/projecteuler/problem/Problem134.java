package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Problem 134: Prime pair connection
 *
 * Consider the consecutive primes p1 = 19 and p2 = 23. It can be verified that,
 * 1219 is the smallest number such that the last digits are formed by p1 whilst also being divisible by p2.
 *
 * In fact, with the exception of p1 = 3 and p2 = 5, for every pair of consecutive primes, p2 > p1,
 * there exist values of n for which the last digits are formed by p1 and n is divisible by p2.
 * Let S be the smallest of these values of n.
 *
 * Find ∑ S for every pair of consecutive primes with 5 ≤ p1 ≤ 1000000.
 *
 * Created by Mikhail Kholodkov
 *         on September 28, 2017.
 */
public class Problem134 extends ProjectEulerProblem {

    public static void main(String[] args) {
        List<Integer> primes = Utils.getPrimesArray(5, 1_000_004); //Next prime after last p1 < 1_000_000 is 1_000_003

        final long[] result = {0L}; //array is just a wrapper to make variable final

        /* Plain and simple brute-force
           Iterating over a pair of primes, looking for appropriate divisor, adding to result.

           I'm 100% sure there's got to be a better algorithm. Probably come back to this in the future.

           Takes about 10 minutes to finish.
         */
        IntStream.iterate(0, i -> i += 1)
                .limit(primes.size() - 1)
                .parallel()
                .forEach(i -> {
                    int prime1 = primes.get(i);
                    int prime2 = primes.get(i + 1);

                    String prime1String = String.valueOf(prime1);

                    for (long j = prime2; ; j += prime2) {

                        String jString = String.valueOf(j);

                        if (jString.endsWith(prime1String)) {
                            result[0] += j;
                            print("Found solution for primes: %s, %s ===> %s", prime1, prime2, j);
                            break;
                }
            }
        });

        printSolution(result[0]);
    }
}