package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.List;


/**
 * Problem 135: Same differences
 *
 * Given the positive integers, x, y, and z, are consecutive terms of an arithmetic progression,
 * the least value of the positive integer, n, for which the equation, x^2 − y^2 − z^2 = n,
 * has exactly two solutions is n = 27:
 *
 * 34^2 − 27^2 − 20^2 = 12^2 − 9^2 − 6^2 = 27
 *
 * It turns out that n = 1155 is the least value which has exactly ten solutions.
 *
 * How many values of n less than one million have exactly ten distinct solutions?
 *
 * Created by Mikhail Kholodkov
 *         on September 30, 2017.
 */
public class Problem135 extends ProjectEulerProblem {

    public static void main(String[] args) {
        int solution = 0; //Final number of possible exactly ten solutions for N < 1_000_000

        /* Based on some empirical research I've discovered that Y is always a divisor of N.
           And relationship between X/Y/Z ~= 0.25

           Assuming these two factors we can quickly iterate through divisors of N,
           By taking a divisor (Y) and taking an arithmetic progression as Y * 0.25.

           By doing so we have X and Z ready.
           Last step is to check if by inserting these values into formula it would give us N.

           That's just a smart brute-force approach. Couple hours of research did not lead me to a better solution.
           Well, if it works and works fast should be enough.

           Running time is a about 4 seconds without printing and about 8 seconds with.
         */
        for (int n = 1; n < 1_000_000; n++) { //1KK is the limit for N

            final int[] currentNSolutionsCount = {0}; //'Final' to support lambda
            List<Integer> divisors = Utils.getDivisors(n); //Getting list of divisors for given N

            if (divisors.size() < 10) { //If there are less than 10 divisors then there will be less than 10 *distinct* solutions
                continue;
            }

            int finalN = n;
            divisors.forEach(y -> {
                for (int x = (int) (y * 1.25);; x++) { //1.25 is a magic coefficient between X, Y and Z. Sort of hack.
                    int progression = x - y;
                    int z = y - progression;

                    if (z < 1) { //If Z goes negative skipping this divisor
                        break;
                    }

                    long result = getResult(x, y, z); //getting equation result

                    if (result > finalN) { //If result is more than N than no solution can be found for given divisor
                        break;
                    }

                    if (result == finalN) { //Exact match. Printing solution and counting total for N
                        if (currentNSolutionsCount[0] == 10) {
                            print("Found valid equation with at least 10 solutions: %s^2 - %s^2 - %s^2 = %s",
                                    x, y, z, finalN);
                        }

                        currentNSolutionsCount[0]++;
                        break;
                    }
                }
            });


            if (currentNSolutionsCount[0] == 10) {
                solution++;
            }
        }

        printSolution(solution);
    }


    /**
     * Returns result for the following equation: x^2 − y^2 − z^2.
     *
     * 34, 27, 20 will return 27
     * 12, 9, 6 will return 27
     */
    private static long getResult(long x, long y, long z) {
        return x * x - y * y - z * z;
    }
}