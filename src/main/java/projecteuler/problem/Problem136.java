package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

import java.util.*;

/**
 * Problem 136: Singleton difference
 *
 * The positive integers, x, y, and z, are consecutive terms of an arithmetic progression.
 * Given that n is a positive integer, the equation, x^2 − y^2 − z^2 = n, has exactly one solution when n = 20:
 *
 * 13^2 − 10^2 − 7^2 = 20
 *
 * In fact there are twenty-five values of n below one hundred for which the equation has a unique solution.
 *
 * How many values of n less than fifty million have exactly one solution?
 *
 * Created by Mikhail Kholodkov
 *         on October 2, 2017.
 */
public class Problem136 extends ProjectEulerProblem {

    private static final int N_LIMIT = 50_000_000;

    public static void main(String[] args) {
        int[] nSolutions = new int[N_LIMIT];

        /* This is almost identical as problem 135. Difference is the upper limit, which makes this one hard
           and impossible for straightforward brute-force approach.

           After some analysis we can rework the given equation x^2 − y^2 − z^2 = n:

           (x+2y)^2 - (x+y)^2 - x^2 = n  ==>  (3y-x)(y+x) = n; where y is an arithmetic progression.

           Here we can notice that x < 3y for n > 0 made this one pretty quick to walk through all N [1..50_000_000].

           Algorithm iterates through values of x and start the y iteration at x/3.
           Breaks out of the y loop if (3y-x)(y+x) >= limit or if y=x/3 then the search is done.

           Completes in just a few seconds!
         */
        int min = 1, progression = 0, n;
        for (int x = 1; x < N_LIMIT * 0.75 && progression != min; x++) { //N/X relationship cannot be greater than 1.25

            min = x / 3;
            progression = min;

            while ((n = (3 * progression - x) * (progression + x)) < N_LIMIT) { //Calculate N for each progression iteration

                if (n > 0) {
                    nSolutions[n]++; //If N is a positive value we can consider that 'some' solution was found
                }

                progression++;
            }
        }

        //Final answer will be a number of N with a single solution
        printSolution(Arrays.stream(nSolutions).parallel().filter(i -> i == 1).count());
    }
}