package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Problem 138: Special isosceles triangles
 *
 * Consider the isosceles triangle with base length, b = 16, and legs, L = 17.
 *
 * By using the Pythagorean theorem it can be seen that the height of the triangle, h = √(17^2 − 8^2) = 15,
 * which is one less than the base length.
 *
 * With b = 272 and L = 305, we get h = 273, which is one more than the base length,
 * and this is the second smallest isosceles triangle with the property that h = b ± 1.
 *
 * Find ∑ L for the twelve smallest isosceles triangles for which h = b ± 1 and b, L are positive integers.
 *
 * Created by Mikhail Kholodkov
 *         on October 4, 2017.
 */
public class Problem138 extends ProjectEulerProblem {

    public static void main(String[] args) {
        /* I brute forced first 7 triangles by using standard Pythagorean theorem [1] (commented unused code below),
           and found an interesting relationship:

           New base = side * 16 + old base, with height alternating + / - 1

           I noticed that the next value of b is related to the last value of L and the b value from two past.

           b = lastL * 16 + oldB

           All of this led me to Pythagorean triples [2], which is enough:

           It becomes simple, we know that b = n^2−m^2; a = 2∗nm and c = n^2 + m^2 (where m < n),
           we also know that b is our case is half of the base of the triangle,
           and this gives us the condition to write the equation:

           Triangle's height H = n^2 − m^2 = 2 ∗ (2 ∗ nm) ± 1 (2 times the half base plus or minus one),
           or better we have to check that n^2 − 4 ∗ (m ∗ n) − m^2 is ±1

           Then I made the most basic loop using this formula and it runs instantly...
           I'm sure there's a nice math explanation for this.

           [1] https://en.wikipedia.org/wiki/Pythagorean_theorem
           [2] https://en.wikipedia.org/wiki/Pythagorean_triple
         */
        long m = 1, sum = 0, count = 0;

        for (long n = 1; n > 0 && count < 12; n++) {

            long difference = ((n * n) - (4 * n * m) - (m * m));

            if (difference == 1 || difference == -1) {
                sum = sum + ((n * n) + (m * m));
                m = n;
                count++;
            }
        }

        printSolution(sum);
    }

    /**
     * This method can be used to brute force approach.
     * Uses Pythagorean theorem to find L which is a hypotenuse.
     *
     * Works perfect for the first 7 triangles, then it runs forever.
     *
     * todo: fix, improve and move it to Utils
     */
    public static void calcNextTriangle(int solutionsCount) {
        for (long base = 2; solutionsCount < 12; base += 2) {

            BigInteger baseHalf = BigInteger.valueOf(base).divide(BigInteger.valueOf(2));

            BigInteger baseHalfSquare = baseHalf.multiply(baseHalf);

            BigInteger baseDecrement = BigInteger.valueOf(base).subtract(BigInteger.ONE);
            BigInteger baseIncrement = BigInteger.valueOf(base).add(BigInteger.ONE);

            BigInteger possibleLegOne = baseHalfSquare.add(baseDecrement.multiply(baseDecrement));
            BigInteger possibleLegTwo = baseHalfSquare.add(baseIncrement.multiply(baseIncrement));

            boolean possibleLegOneIsSquareInteger = Utils.isInteger(Utils.sqrt(new BigDecimal(possibleLegOne.doubleValue()), 10).doubleValue());
            boolean possibleLegTwoIsSquareInteger = Utils.isInteger(Utils.sqrt(new BigDecimal(possibleLegTwo.doubleValue()), 10).doubleValue());

            if (possibleLegOneIsSquareInteger || possibleLegTwoIsSquareInteger) {
                print("Found new special isosceles triangle with base: %s, legs: %s, height is: %s",
                        base, possibleLegOneIsSquareInteger ? Utils.sqrt(new BigDecimal(possibleLegOne.doubleValue()), 10) :
                                Utils.sqrt(new BigDecimal(possibleLegTwo.doubleValue()), 10),
                        base + (possibleLegOneIsSquareInteger ? -1 : 1));
                solutionsCount++;
            }
        }
    }


}