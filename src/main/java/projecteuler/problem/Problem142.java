package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

/**
 * Problem 142: Perfect Square Collection
 *
 * Find the smallest x + y + z with integers x > y > z > 0 such that
 * x + y, x − y, x + z, x − z, y + z, y − z are all perfect squares.

 * Created by Mikhail Kholodkov
 *         on October 5, 2017.
 */
public class Problem142 extends ProjectEulerProblem {

    public static void main(String[] args) {
        boolean solutionFound = false;

        /* Basically a bit enhanced brute-force method.

           Enhanced empirically in terms of Y and Z should always be even values,
           otherwise first four conditions will never be true.

           Performance completely relies on isSquare() method implementation.
           As a part of this solution method was greatly improved, thus solution was found in couple minutes.
         */
        for (int x = 3; !solutionFound; x++) { //Iterating X until solution is found

            if (x % 10000 == 0) {
                print("So far x = %s...", x); //Intermediate output every 10k of X
            }

            //Inner loop to go through Y
            for (int y = 4; y < x; y += 2) {

                int xPlusY = x + y;
                if (!Utils.isSquare(xPlusY)) {
                    continue;
                }

                int xMinusY = x - y;
                if (!Utils.isSquare(xMinusY)) {
                    continue;
                }

                //Inner loop to go through Z
                for (int z = 2; z < y; z += 2) {

                    int xPlusZ = x + z;
                    if (!Utils.isSquare(xPlusZ)) {
                        continue;
                    }

                    int xMinusZ = x - z;
                    if (!Utils.isSquare(xMinusZ)) {
                        continue;
                    }

                    int yPlusZ = y + z;
                    if (!Utils.isSquare(yPlusZ)) {
                        continue;
                    }

                    int yMinusZ = y - z;
                    if (Utils.isSquare(yMinusZ)) { //If final condition is true - solution is found

                        lineBreak();
                        print("Found 4 squares for x = %s and y = %s z = %s. " +
                                        "x - y = %s, x + y = %s, x + z = %s, x - z = %s, y + z = %s, y -z = %s",
                                x, y, z, xMinusY, xPlusY, xPlusZ, xMinusZ, yPlusZ, yMinusZ);

                        solutionFound = true;
                        printSolution(x + y + z);
                    }
                }
            }
        }
    }
}