package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

import java.util.stream.IntStream;

/**
 * Problem 145: How many reversible numbers are there below one-billion?
 *
 * Some positive integers n have the property that the sum [ n + isReversible(n) ] consists entirely of odd (decimal) digits.
 * For instance, 36 + 63 = 99 and 409 + 904 = 1313.
 *
 * We will call such numbers reversible; so 36, 63, 409, and 904 are reversible.
 * Leading zeroes are not allowed in either n or isReversible(n).
 *
 * There are 120 reversible numbers below one-thousand.
 *
 * How many reversible numbers are there below one-billion (109)?

 * Created by Mikhail Kholodkov
 *         on October 10, 2017.
 */
public class Problem145 extends ProjectEulerProblem {

    private static final int MAX_VALUE = 1_000_000_000;

    public static void main(String[] args) {

        /* Very easy brute-force for such a high number problem.

           We can obviously eliminate half of values, since number and its reversed value will match the condition.
           We can also skip 1 digit values.

           Empirically it can be noticed that there are no 5 and 9-digits solutions either (not sure about math reasoning),
           by eliminating them, running time reduces to one second.

           Takes less than 10 seconds to complete.
         */
        long result = IntStream.range(11, MAX_VALUE)
                .parallel()
                .filter(Problem145::isReversible)
                //.peek(i -> print("Found new reversible number: %s", i))
                .count();

        printSolution(result * 2); //result multiplied by 2, since we were skipping even numbers
    }

    /**
     * Method checks if provided value can be reversed and after summarizing it with original value
     * will be containing only odd digits [1,3,5,7,9]
     *
     * Even values will always return false in order to iterate only odd numbers.
     */
    private static boolean isReversible(int value) {
        if (value % 2 == 0) {
            return false;
        }

        int temp = value;
        int result = 0;

        while (temp > 0) {
            result = (result * 10) + (temp % 10);
            temp /= 10;
        }

        result += value;

        while (((result % 10) & 1) > 0) {
            result /= 10;
        }

        return result == 0;
    }
}