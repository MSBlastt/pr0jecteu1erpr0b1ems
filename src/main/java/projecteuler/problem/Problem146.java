package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.LongStream;

/**
 * Problem 145: How many reversible numbers are there below one-billion?
 *
 * The smallest positive integer n for which the numbers n^2+1, n^2+3, n^2+7, n^2+9, n^2+13, and n^2+27 are consecutive primes is 10.
 * The sum of all such integers n below one-million is 1242490.
 *
 * What is the sum of all such integers n below 150 million?
 *
 * Created by Mikhail Kholodkov
 *         on October 11, 2017.
 */
public class Problem146 extends ProjectEulerProblem {

    private static final long LIMIT = 150_000_000;

    private static final List<Integer> TERMS_LIST = Arrays.asList(1, 3, 7, 9, 13, 27);

    private static final List<Integer> NON_PRIMES_TERMS_LIST = Arrays.asList(5, 11, 15, 17, 19, 21, 25);

    public static void main(String[] args) {
        /* It's immediately obvious that then only valid combination for consecutive primes would be:
           X01, X03, X07, X09, X13, X27 pattern.

           Three conditions should be met to find the correct N values:

           - N^2 should ends with ...00. Therefore only squares of N mod 10 would satisfy condition.
           - N^2 should not be multiplies of 3, 7, 9, 13, 27, otherwise one of the equation's values will not be a prime.
           - N^2 + [5, 11, 15, 17, 19, 21, 25] should not be primes, otherwise it ruins the sequence.

           Everything else is depends on the speed of primality test.

           Takes about 15-30 seconds to finish (based on amount of available cores)
         */
        printSolution(
                LongStream.iterate(10, n -> n += 10)
                .limit(LIMIT / 10)
                .parallel()
                .map(n -> n * n) // Getting a square of N
                .filter(Problem146::checkPrimeDivisors) // Check for divisors
                .filter(Problem146::checkForPrimality) // Check for primality nSquare + [terms]
                .filter(Problem146::checkForNonPrimality) // Check for non-primality for 19 and 21 terms
                .peek(Problem146::printFoundNSolution) // All conditions are met, we found a valid N!
                .map(n -> (long) Math.sqrt(n)) // Retrieving original N back from a square
                .sum()); // Summarizing result
            }

    private static boolean checkPrimeDivisors(long nSquare) {
        return Utils.getGreatestCommonDivisor((int) Math.sqrt(nSquare), 3 * 7 * 9 * 13 * 27) == 1;
    }

    private static boolean checkForPrimality(long nSquare) {
        return TERMS_LIST
                .stream()
                .mapToLong(t -> nSquare + t)
                .allMatch(Utils::isPrimeMillerRabin);
    }

    private static boolean checkForNonPrimality(long nSquare) {
        return NON_PRIMES_TERMS_LIST
                .stream()
                .mapToLong(t -> nSquare + t)
                .noneMatch(Utils::isPrimeMillerRabin);
    }

    private static void printFoundNSolution(long nSquare) {
        print("Found N = %s, for which equation values are all consecutive primes: %s",
                (long) Math.sqrt(nSquare), Arrays.toString(TERMS_LIST.stream().mapToLong(t -> t + nSquare).toArray()));
    }
}