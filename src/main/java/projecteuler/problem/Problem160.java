package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

/**
 * Problem 160: Factorial trailing digits
 *
 * For any N, let f(N) be the last five digits before the trailing zeroes in N!.
 * For example,
 *
 * 9! = 362880 so f(9)=36288
 * 10! = 3628800 so f(10)=36288
 * 20! = 2432902008176640000 so f(20)=17664
 *
 * Find f(1,000,000,000,000)
 *
 * Created by Mikhail Kholodkov
 *         on October 12, 2017.
 */
public class Problem160 extends ProjectEulerProblem {


    public static void main(String[] args) {

        /* By analysis (actually took me a couple of evenings), I've noticed the main Key to solve it:

           1 trillion (or 10^12) has the following prime factorization: 2^12 × 5^12 (Just Googled it).
           It means that, when n is a positive multiple of 2500, f(n) = f(n * 5^x) <==> f(x) = f(5x), for all x >= 0.

           This means it'll be sufficient to compute f(2560000) instead of f(10^12), which is rather fast.

           Additionally all trailing zeroes can be omitted for the next iteration of computation.

           Altogether answer comes instantaneously.
         */
        long result = 1; // Base multiplier

        for (long i = 1; i <= 2_560_000; i++) {

            result *= i;

            while ((result % 10) == 0) { // Omitting trailing zeroes from the end
                result /= 10;
            }

            // Reducing intermediate result by getting only a 10^12 division's remainder, since we don't need more than 12 digits for calculation
            result %= 1_000_000_000_000L;
        }

        String solution = String.valueOf(result);
        printSolution(solution.substring(solution.length() - 5)); // Getting only last 5 digits
    }
}