package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

/**
 * Problem 164: Numbers for which no three consecutive digits have a sum greater than a given value
 *
 * How many 20 digit numbers n (without any leading zero) exist such that no three consecutive digits of n have a sum greater than 9?
 *
 * Created by Mikhail Kholodkov
 *         on October 25, 2017.
 */
public class Problem164 extends ProjectEulerProblem {

    private static final int MAX_SUM = 9;
    private static final int DIGITS_COUNT = 20;

    public static void main(String[] args) {

        /* In the beginning we can generate all the 3 digit numbers such that the sum of their digits is less than 10.
           After that, a simple algorithm iterates using the last 2 old digits and looking for the possible new ones.
         */
        int firstDigit, secondDigit, thirdDigit, tempDigitSum;

        long[][] lastDigitsMatrixTemp, lastDigitsMatrixResult = new long[10][10];

        // Looking for all possible 3 digits combinations
        for (int x = 100; x < 1000; x++) {

            firstDigit = x / 100;
            secondDigit = (x % 100) / 10;
            thirdDigit = x % 10;

            if (firstDigit + secondDigit + thirdDigit < 10) {
                ++lastDigitsMatrixResult[secondDigit][thirdDigit];
            }
        }

        // Filling the result matrix with possible values (sort of dynamic programming)
        for (int d = 3; d < DIGITS_COUNT; d++) {

            lastDigitsMatrixTemp = lastDigitsMatrixResult;
            lastDigitsMatrixResult = new long[10][10];

            for (firstDigit = 0; firstDigit <= MAX_SUM; firstDigit++) {

                for (secondDigit = 0; secondDigit <= MAX_SUM; secondDigit++) {

                    tempDigitSum = MAX_SUM - secondDigit - firstDigit;

                    for (thirdDigit = 0; thirdDigit <= tempDigitSum; thirdDigit++) {
                        lastDigitsMatrixResult[secondDigit][thirdDigit] += lastDigitsMatrixTemp[firstDigit][secondDigit];
                    }
                }
            }
        }

        // Counting solutions
        long sum = 0;
        for (firstDigit = 0; firstDigit <= MAX_SUM; firstDigit++) {

            for (secondDigit = 0; secondDigit <= MAX_SUM; secondDigit++) {
                sum += lastDigitsMatrixResult[firstDigit][secondDigit];
            }
        }

        printSolution(sum);
    }
}