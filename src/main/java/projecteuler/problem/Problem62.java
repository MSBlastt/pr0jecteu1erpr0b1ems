package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Problem 62: Cubic Permutations
 *
 * The cube, 41063625 (345^3), can be permuted to produce two other cubesByLength: 56623104 (384^3) and 66430125 (405^3).
 * In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.
 *
 * Find the smallest cube for which exactly five permutations of its digits are cube.
 *
 * Created by Mikhail Kholodkov
 *         on June 8, 2017.
 */
public class Problem62 extends ProjectEulerProblem {

    private static Map<Integer, Set<Long>> cubesByLength = new HashMap<>();

    public static void main(String[] args) {
        //Adding dummy Sets to Map
        for (int i = 1; i < 20; i++) {
            cubesByLength.put(i, new HashSet<>());
        }

        //Looking for all possible cubes within Long's range
        for (int i = 1; i < Math.cbrt(Long.MAX_VALUE); i++) {
            long cube = (long) Math.pow(i, 3);
            int powLength = Long.toString(cube).length();

            cubesByLength.get(powLength).add(cube);
        }

        Collection<Set<Long>> cubesValues = cubesByLength.values();

        System.out.println("Added " + cubesValues.stream().mapToInt(Set::size).sum() + " cubes to the map!");
        lineBreak();

        /*
            Iterating over found cubes in ascending order. Trying to find all possible permutations for each,
            within the same integer length (i.e. 2 for 27, 3 for 125 etc), within the same 'cubesByLength' map.
            If found, adding to permutations collection, if found exactly 5 - this is the answer.
         */
        for (Long cube : cubesValues.stream().flatMap(Collection::stream).collect(Collectors.toCollection(TreeSet::new))) {

            Set<Long> permutations = getPossibleCubePermutations(cube);

            if (permutations.size() == 5) {
                printSolution("Found it! " + permutations.stream().min(Comparator.naturalOrder()).orElse(0L));
                break;
            }
        }
    }

    private static Set<Long> getPossibleCubePermutations(long pow) {
        String inputCube = Long.toString(pow);

        return cubesByLength.get(inputCube.length())
                .stream()
                .filter(c -> Utils.isPossiblePermutation(c.toString(), inputCube))
                .collect(Collectors.toSet());
    }

}
