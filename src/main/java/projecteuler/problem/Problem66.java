package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Problem 66: Diophantine Equation
 *
 * Consider quadratic Diophantine equations of the form:
 *
 * x2 – Dy2 = 1
 *
 * For example, when D=13, the minimal solution in x is 6492 – 13×1802 = 1.
 *
 * It can be assumed that there are no solutions in positive integers when D is square.
 *
 * By finding minimal solutions in x for D = {2, 3, 5, 6, 7}, we obtain the following:
 *
 * 32 – 2×22 = 1
 * 22 – 3×12 = 1
 * 92 – 5×42 = 1
 * 52 – 6×22 = 1
 * 82 – 7×32 = 1
 *
 * Hence, by considering minimal solutions in x for D ≤ 7, the largest x is obtained when D=5.
 *
 * Find the value of D ≤ 1000 in minimal solutions of x for which the largest value of x is obtained.
 *
 * Created by Mikhail Kholodkov
 *         on June 10, 2017.
 */
public class Problem66 extends ProjectEulerProblem {


    public static void main(String[] args) {
        BigInteger maxX = BigInteger.ZERO;
        int dForMaxX = 0;

        //Iterating natural number D <= 1000
        for (int d = 13; d < 10010; d++) {

            //Square number does not have a solution for positive X, Y
            if (Utils.isSquare(d)) {
                continue;
            }

            /*
             * X^2 – D * Y^2 = 1 is also known as Pell's Equation.
             *
             * It appears that: Sqrt(d) ~= x/y (minimal or fundamental solution),
             * by assuming this we can throw out a huge chunk of possible x and y values.
             *
             * The following algorithm thoroughly described here: [1]. Additional information here: [2]
             *
             * It uses the unique sequence of convergents to the regular continued fraction for Sqrt(d).
             * Thus, the fundamental solution will be found by performing the continued fraction expansion and
             * testing each successive convergent until a solution is found.
             *
             * [1] http://mathworld.wolfram.com/PellEquation.html
             * [2] https://en.wikipedia.org/wiki/Pell%27s_equation#Fundamental_solution_via_continued_fractions
             */
            int dRoot = (int) Math.sqrt(d);

            int dTemp = d - (dRoot * dRoot);
            int n = ((dRoot + dRoot) % dTemp) - dRoot;
            int m = (dRoot + dRoot) / dTemp;

            List<String> expansion = new ArrayList<>();
            expansion.add(0, Integer.toString(dRoot));

            while (true) {

                expansion.add(0, Integer.toString(m));
                BigInteger x = BigInteger.valueOf(m), y = BigInteger.ONE;

                for (int j = 1; j < expansion.size(); j++) {
                    BigInteger temp = x;
                    x = x.multiply(BigInteger.valueOf(Long.parseLong(expansion.get(j)))).add(y);
                    y = temp;
                }

                if (calcEquation(d, x, y) == 1) {
                    System.out.println(
                            String.format("Found solution for D = %s. Formula: %s^2 - %s * %s^2 = 1", d, x, d, y));

                    if ((maxX = maxX.max(x)).equals(x)) {
                        dForMaxX = d;
                    }

                    break;
                }

                dTemp = (d - (n * n)) / dTemp;
                m = (dRoot - n) / dTemp;
                n = ((dRoot - n) % dTemp) - dRoot;
            }
        }

        printSolution(String.format("D = %s, max X = %s", dForMaxX, maxX.toString()));
    }

    private static int calcEquation(int d, BigInteger x, BigInteger y) {
        return x.multiply(x).subtract(y.multiply(y).multiply(BigInteger.valueOf(d))).intValue();
    }

}