package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

/**
 * Problem 69: Totient maximum
 *
 * Euler's Totient function, φ(n) [sometimes called the phi function],
 * is used to determine the number of numbers less than n which are relatively prime to n.
 * For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, φ(9)=6.
 *
 * n	Relatively Prime	φ(n)	n/φ(n)
 * 2	1	                1	    2
 * 3	1,2	                2	    1.5
 * 4	1,3	                2	    2
 * 5	1,2,3,4	            4	    1.25
 * 6	1,5	                2	    3
 * 7	1,2,3,4,5,6	        6	    1.1666...
 * 8	1,3,5,7	            4	    2
 * 9	1,2,4,5,7,8	        6	    1.5
 * 10	1,3,7,9	            4	    2.5
 *
 * It can be seen that n=6 produces a maximum n/φ(n) for n ≤ 10.
 *
 * Find the value of n ≤ 1,000,000 for which n/φ(n) is a maximum.
 *
 * Created by Mikhail Kholodkov
 *         on June 14, 2017.
 */
public class Problem69 extends ProjectEulerProblem {


    public static void main(String[] args) {
        int maxN = 1_000_001;
        double solutionPhi = 0d;
        int solutionN = 0;

        for (int n = 2; n < maxN; n++) {
            int result = getPhi(n);

            double phiDividesN = n / (double) result;
            if (phiDividesN > solutionPhi) {
                solutionPhi = phiDividesN;
                solutionN = n;
                System.out.println(String.format("Found new solution for N = %s, current max: %s", n, phiDividesN));
            }
        }

        printSolution(solutionN);
    }

    /**
     * Returns Euler's Totient function, φ(n) [sometimes called the phi function]
     * Represents amount of numbers less than N which are relatively prime to N.
     *
     * More info: [1]
     *
     * For example φ(10) = 4 (10 has 4 relative primes: 1, 3, 7 ,9).
     *
     * To calculate phi, N needs to be divided on largest prime divisor (LPD) and then
     * multiplied on pre-decremented LPD. The following algorithm does exactly that.
     *
     * [1] - https://en.wikipedia.org/wiki/Euler%27s_totient_function
     */
    protected static int getPhi(int n) {
        int tot = n; //this will be the totient at the end of the sample
        for (int p = 2; p * p <= n; p++) {

            if (n % p == 0) {

                tot /= p;
                tot *= (p - 1);

                while (n % p == 0) {
                    n /= p;
                }
            }
        }

        if (n > 1) { // now n is the largest prime divisor
            tot /= n;
            tot *= (n - 1);
        }

        return tot;
    }
}