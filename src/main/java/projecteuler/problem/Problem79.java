package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.*;

/**
 * Problem 79: Passcode derivation
 *
 * A common security method used for online banking is to ask the user for three random characters from a passcode.
 * For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.
 * 
 * The text file, keylog.txt, contains fifty successful login attempts.
 * 
 * Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
 * 
 * 
 * Created by Mikhail Kholodkov
 *         on June 18, 2017.
 */
public class Problem79 extends ProjectEulerProblem {

    private static String keylog =  "319,680,180,690,129," +
                                    "620,762,689,762,318," +
                                    "368,710,720,710,629," +
                                    "168,160,689,716,731," +
                                    "736,729,316,729,729," +
                                    "710,769,290,719,680," +
                                    "318,389,162,289,162," +
                                    "718,729,319,790,680," +
                                    "890,362,319,760,316," +
                                    "729,380,319,728,716";

    public static void main(String[] args) {
        //Frequency/order map
        Map<Integer, Map<Integer, Integer>> map = new HashMap<Integer, Map<Integer, Integer>>() {{
            put(1, new HashMap<>());
            put(2, new HashMap<>());
            put(3, new HashMap<>());
        }};

        //Adding values based on number of their appearance in the keyLog
        for (String key : keylog.split(",")) {
            String[] digits = key.split("");
            for (int i = 1; i < 4; i++) {
                Map<Integer, Integer> innerMap = map.get(i);

                Integer intValue = Integer.valueOf(digits[i - 1]);

                innerMap.put(intValue, innerMap.get(intValue) != null ? innerMap.get(intValue) + 1 : 1);
            }
        }

        System.out.println("Frequency of digits by appearance: " + map);

        lineBreak();

        //Sorting by frequency
        List<SortedSet<Map.Entry<Integer, Integer>>> sortedSetList = new ArrayList<>();
        List<Integer> digits = new ArrayList<>();

        for (int i = 1; i < 4; i++) {
            SortedSet<Map.Entry<Integer, Integer>> sortedSet = entriesSortedByValues(map.get(i));
            sortedSetList.add(sortedSet);
            sortedSet.forEach(c -> digits.add(c.getKey()));

            System.out.println("Most common values for " + i + " digit are: " + sortedSet);
        }

        //Finding a passcode length (assuming only distinctive values - !may be false assumption!)
        long passcodeLength = digits.stream().distinct().count();
        lineBreak();
        System.out.println("Assuming shortest possible passcode is the length of " + passcodeLength + ", by counting distinctive digits.");

        //Finding a passcode based on frequency/order in all three parts
        Set<String> passCodes = new LinkedHashSet<>();

        Map<Integer, Integer> total = new LinkedHashMap<>();

        for (SortedSet<Map.Entry<Integer, Integer>> set : sortedSetList) {
            Iterator<Map.Entry<Integer, Integer>> iterator = set.iterator();
            for (int i = 0; i < set.size(); i++) {
                Map.Entry<Integer, Integer> entry = iterator.next();
                total.put(entry.getKey(), total.get(entry.getKey()) != null ? total.get(entry.getKey()) + entry.getValue() : entry.getValue());
            }
        }

        lineBreak();
        System.out.println("Total frequency with overlapping successful attempts: " + total);

        passCodes.add(String.valueOf(total.keySet().toString().replaceAll("[^0-9]", "")));

        lineBreak();
        System.out.println("Most likely the passcode is: " + passCodes.iterator().next());

        //Searching for additional possible pass codes based on low delta of frequency/order, comparing to the next digit in originally found passcode
        Iterator<Map.Entry<Integer, Integer>> iterator = total.entrySet().iterator();
        Map.Entry<Integer, Integer> current = iterator.next();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Integer> next = iterator.next();
            if (Utils.getDelta(current.getValue(), next.getValue()) < 5) {
                List<String> newPassCodes = new ArrayList<>();
                for (String passcode : passCodes) {
                    newPassCodes.add(Utils.swapCharacters(passcode, current.getKey(), next.getKey())); //swap chars
                }
                passCodes.addAll(newPassCodes);
            }
            current = next;
        }

        printEmptyLine();
        System.out.println("Additional possible answers:");
        passCodes.stream().skip(1).forEach(System.out::println);
    }



    private static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<>(
                (e1, e2) -> {
                    int res = e2.getValue().compareTo(e1.getValue());
                    return res != 0 ? res : 1; // Special fix to preserve items with equal values
                }
        );
        sortedEntries.addAll(map.entrySet());

        return sortedEntries;
    }
}
