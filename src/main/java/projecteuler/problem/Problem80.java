package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigDecimal;
import java.util.stream.IntStream;

/**
 * Problem 80: Square root digital expansion
 *
 * It is well known that if the square root of a natural number is not an integer, then it is irrational.
 * The decimal expansion of such square roots is infinite without any repeating pattern at all.
 *
 * The square root of two is 1.41421356237309504880...,
 * and the digital sum of the first one hundred decimal digits is 475.
 *
 * For the first one hundred natural numbers,
 * find the total of the digital sums of the first one hundred decimal digits for all the irrational square roots.
 * 
 * Created by Mikhail Kholodkov
 *         on June 20, 2017.
 */
public class Problem80 extends ProjectEulerProblem {

    public static void main(String[] args) {
        //'Effectively final' variables
        final int[] totalSum = {0};
        final int[] sum = {0};

        IntStream.range(1, 101) //Searching for irrational square roots of a first 100 natural numbers
                .filter(i -> !Utils.isSquare(i)) //Skipping number If square root isn't irrational
                .forEach(i -> {
                    Utils.sqrt(BigDecimal.valueOf(i), 99) //Getting value with 99 digits of a remainder (+1 digit of an integer part)
                            .toString().replace(".", "") //Removing dot character
                            .chars()
                            .mapToObj(c -> (char) c)
                            .forEach(c -> sum[0] += Integer.valueOf(String.valueOf(c))); //Summarizing each digit

                    totalSum[0] += sum[0]; //Summarizing with final sum
                    sum[0] = 0;

                    System.out.println(String.format("For number: %s, square root's remainder sum is: %s", i, sum[0]));
                });

        printSolution(totalSum[0]);
    }
}