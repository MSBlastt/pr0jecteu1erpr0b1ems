package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

/**
 * Problem 85: Counting rectangles
 *
 * By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles.
 *
 * Although there exists no rectangular grid that contains exactly two million rectangles,
 * find the area of the grid with the nearest solution.
 * 
 * Created by Mikhail Kholodkov
 *         on June 21, 2017.
 */
public class Problem85 extends ProjectEulerProblem {

    public static void main(String[] args) {
        int goal = 2_000_000, upperLimit = 2_100_000;
        int closestDelta = goal, closestMatch = 0, closestMatchX = 0, closestMatchY = 0;

        /*  Looping from 1 to 100.
            Since minimal step for countSubRectanglesInsideGrid(101, ++y) > 10_000, which is unlikely a solution.
         */
        for (int x = 1, delta, rectanglesCount; x < 100; x++) {
            for (int y = 1; y < 100 && (rectanglesCount = countSubRectanglesInsideGrid(x, y)) < upperLimit; y++) {

                //Getting delta between current rectangles count and goal, looking for lowest
                delta = Utils.getDelta(rectanglesCount, goal);

                if (closestDelta > delta) {
                    closestDelta = delta;
                    closestMatch = rectanglesCount;
                    closestMatchX = x;
                    closestMatchY = y;
                }

                System.out.println("For X * Y = " + x + " * " + y + ", count is: " + rectanglesCount);
            }
        }

        printSolution(String.format("%s. For x = %s, y = %s with sub-rectangles count = %s, delta = %s against goal = %s.",
                closestMatchX * closestMatchY, closestMatchX, closestMatchY, closestMatch, closestDelta, goal));
    }

    /**
     * Formula m(m+1)n(n+1)/4. Returns amount of sub-rectangles in a grid.
     *
     * A rectangle is defined by its two projections on the x-axis and on the y-axis.
     * Projection on x-axis : number of pairs (a, b) such that 1 <= a <= b <= m = m(m+1)/2,
     * same applies for y-axis.
     *
     * 1, 1 returns 1
     * 3, 2 returns 18
     * 10, 16 returns 7480
     */
    private static int countSubRectanglesInsideGrid(int x, int y) {
        return x * (x + 1) * y * (y + 1) / 4;
    }
}