package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.*;

/**
 * Problem 87: Prime power triples
 *
 * The smallest number expressible as the sum of a prime square, prime cube, and prime fourth power is 28. In fact,
 * there are exactly four numbers below fifty that can be expressed in such a way:
 *
 * 28 = 2^2 + 2^3 + 2^4
 * 33 = 3^2 + 2^3 + 2^4
 * 49 = 5^2 + 2^3 + 2^4
 * 47 = 2^2 + 3^3 + 2^4
 *
 * How many numbers below fifty million can be expressed as the sum of a prime square, prime cube, and prime fourth power?
 *
 * Created by Mikhail Kholodkov
 *         on June 27, 2017.
 */
public class Problem87 extends ProjectEulerProblem {

    private static final int LIMIT = 50_000_000;

    //Memoization map. Contains reusable combinations of a "prime.power" = value (i.e. 7.2 = 49, 11.3 = 1331, etc)
    private static final HashMap<String, Long> primePowers = new HashMap<>();

    public static void main(String[] args) {
        List<Long> primes = Utils.getPrimesArray(3L, 7100L); // 7100 is a limit, since 7100 ^ 2 > 50_000_000

        //Contains a final list of all possible numbers
        Set<Long> result = new HashSet<>();

        /* Iterating through pre-filled map of Primes, starting from fourth power, then cube, then square.
           Stream pipeline goes from incrementing fourth powers down to incrementing squares.
           Going backwards by iterating fourth powers first is the fastest way to reach the limit (50kk)
        */
        primes.stream()
                .filter(prime -> belowLimit(powerLongAndSaveToMap(prime, 4)))
                .forEach(primeFourth -> {

                    Long fourth = powerLongAndSaveToMap(primeFourth, 4);

                    primes.stream()
                            .filter(prime -> belowLimit(powerLongAndSaveToMap(prime, 3)))
                            .forEach(primeCube -> {

                                Long cube = powerLongAndSaveToMap(primeCube, 3);

                                primes.stream()
                                        .filter(prime -> {
                                            Long square = powerLongAndSaveToMap(prime, 2);
                                            Long sum = square + cube + fourth;

                                            //Filtering any sum which is higher than the limit or was added before.
                                            return belowLimit(sum) && !result.contains(sum);
                                        })
                                        .forEach(primeSquare -> {

                                            //It is save to call method again, since value is stored in map
                                            Long square = powerLongAndSaveToMap(primeSquare, 2);
                                            Long sum = square + cube + fourth;

                                            //Output is huge, thus commented out. Will slow down solution for  ~15 seconds.
                                            /*System.out.println(String.format("Found solution %s = %s^2 + %s^3 + %s^4",
                                                    sum, primeFourth, primeCube, primeSquare));
                                            */

                                            result.add(sum);
                                        });
                            });
                });

        printSolution(result.size());
    }

    /**
     * Powers provided value for the given power.
     * Product will be stored in the map in order to retrieve same value later (Memoization).
     */
    private static long powerLongAndSaveToMap(long value, int power) {
        String key = String.valueOf(value + "." + power);
        if (primePowers.containsKey(key)) {
            return primePowers.get(key);
        }

        long sum = value;
        for (int i = 2; i <= power; i++) {
            sum *= value;

            long tempSum = sum;
            primePowers.putIfAbsent(String.valueOf(value + "." + i), tempSum);
        }

        return sum;
    }

    /**
     * Returns true if given value is less than the limit (50kk)
     */
    private static boolean belowLimit(Long input) {
        return input < LIMIT;
    }
}
