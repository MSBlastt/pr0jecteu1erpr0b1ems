package projecteuler.problem;

import projecteuler.ProjectEulerProblem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Problem 88: Product-sum numbers
 *
 * A natural number, N, that can be written as the sum and product of a given set of at least two natural numbers, {a1, a2, ... , ak} is called a product-sum number: N = a1 + a2 + ... + ak = a1 × a2 × ... × ak.
 * 
 * For example, 6 = 1 + 2 + 3 = 1 × 2 × 3.
 * 
 * For a given set of size, k, we shall call the smallest N with this property a minimal product-sum number. The minimal product-sum numbers for sets of size, k = 2, 3, 4, 5, and 6 are as follows.
 * 
 * k=2: 4 = 2 × 2 = 2 + 2
 * k=3: 6 = 1 × 2 × 3 = 1 + 2 + 3
 * k=4: 8 = 1 × 1 × 2 × 4 = 1 + 1 + 2 + 4
 * k=5: 8 = 1 × 1 × 2 × 2 × 2 = 1 + 1 + 2 + 2 + 2
 * k=6: 12 = 1 × 1 × 1 × 1 × 2 × 6 = 1 + 1 + 1 + 1 + 2 + 6
 * 
 * Hence for 2≤k≤6, the sum of all the minimal product-sum numbers is 4+6+8+12 = 30; note that 8 is only counted once in the sum.
 * 
 * In fact, as the complete set of minimal product-sum numbers for 2≤k≤12 is {4, 6, 8, 12, 15, 16}, the sum is 61.
 * 
 * What is the sum of all the minimal product-sum numbers for 2≤k≤12000?
 * 
 * Created by Mikhail Kholodkov
 *         on June 29, 2017.
 */
public class Problem88 extends ProjectEulerProblem {

    private static Long product, sum;

    private static List<Integer> array;

    public static void main(String[] args) {
        //Final list of sum for each K
        List<Long> listOfK = new ArrayList<>();

        for (int k = 2; k <= 12000; k++) {

            //Generating array with default values
            array = fillArrayAndGetArray(k);

            //Sum equal to the size of array at this point, since all values are ones ('1')
            sum = (long) array.size();
            product = 1L;

            Long result = 0L;

            //Overloaded array means that increment values to the left from the highest is no longer meaningful,
            //due to max value being more that the size of array (any product will be higher than sum at this point)
            boolean isArrayOverloaded = false;

            /* Algorithm increments array values starting from the right, goes to the left, one by one.
               If product becomes higher than sum at some point, array will be reduced to the point when it's not.
               Additional info on reduction part can be found in comments below.
               After every incrementation product and sum of elements are being calculated, if they're equal - solution is found.
               When solution is found for array of given size of K, it saves the sum/product to the list for future use.
               This algorithm is far from being perfect, takes about 4 minutes to complete for k <=12000.
             */
            while (!isArrayOverloaded) {

                //Incrementing array values until we'll find a match of product and sum
                while (product.compareTo(sum) != 0 && !isArrayOverloaded) {
                    incrementArrayValues();
                    isArrayOverloaded = array.get(array.size() - 1) > array.size();
                }

                //Solution is found for given K
                if (product.compareTo(sum) == 0 && (result == 0 || result > product)) {
                    result = product;

                    //Additional console print every 1000 divisible K, to keep track of algorithm execution
                    if (k % 1000 == 0) {
                        print("Latest Found solution for k = %s, product = sum = %s. Array values: %s",
                                k, result, omitRepeatableOnes(array));
                    }
                }

                //Updating sum and product if solution was found, but result < product
                if (!isArrayOverloaded) {
                    incrementRightmostMinValue();
                }
            }

            //Adding solution to the list of solutions for each K
            listOfK.add(result);
        }

        Long solution = listOfK.stream().distinct().mapToLong(l -> l).sum();

        printSolution(solution);
    }

    /**
     * Returns array filled with K size ones ('1'),
     */
    private static List<Integer> fillArrayAndGetArray(int k) {
        List<Integer> array = new ArrayList<>(k);
        for (int i = 1; i <= k; i++) {
            array.add(1);
        }
        return array;
    }

    /**
     * Will increment array values by 1, and will reduce it, if necessary.
     */
    private static void incrementArrayValues() {
        while (sum > product) {
            incrementRightmostMinValue();
        }

        sum = calcSum(array);

        reduceArray();
    }

    /**
     * Increments rightmost value in array.
     * Rightmost means the value which is less than max (array[max]), and is equal to min (array[0]),
     * by iterating backwards from highest to lowest.
     *
     * For example: [1, 1, 1, 2, 3, 4, 5] -> [1, 1, 2, 2, 3, 4, 5].
     */
    private static void incrementRightmostMinValue() {
        int min = array.get(0);

        for (int i = array.size() - 1; i >= 0; i--) {
            if (array.get(i) == min) {
                array.set(i, min + 1);

                product *= 2;
                if (product > sum) {
                    break;
                }
            }
        }
    }

    /**
     * Decreases array overall product/sum values, if product is bigger than sum,
     * otherwise we found a solution.
     * Needs to be called in order to avoid incrementing array values when it's no longer meaningful.
     * Reducing array means to increment the 'specific' value and reset all other value to the left of it.
     * 'specific' value can be found by multiplying array values one by one, until product becomes bigger than sum.
     * Such a value is the last value of an array, multiplying which will not make array values product bigger than sum.
     */
    private static void reduceArray() {
        Long tempPow = Long.valueOf(array.get(array.size() - 1));

        for (int i = array.size() - 2; i >= 0; i--) {
            tempPow *= array.get(i);


            if (tempPow.compareTo(sum) > 0) {
                Integer e = array.get(i + 1) + 1;
                array.set(i + 1, e);

                int indexToReset = i + 1;
                if (i != array.size() - 2) {
                    List<Integer> temp = getSubList(i);

                    sortArray(temp);
                    indexToReset = getIndexToReset(e, temp);

                    iterateSortedSubList(i, temp);
                }

                if (e > 2) {
                    resetArrayUntilE(array, indexToReset);
                }

                sum = calcSum(array);
                product = calcProductLimited(sum);

                break;
            }
        }
    }

    /**
     * Returns subList of main array based on provided index.
     */
    private static List<Integer> getSubList(int i) {
        return array.subList(i, array.size());
    }

    /**
     * Iterates through sorted sublist starting from provided index, and replaces values in main array.
     */
    private static void iterateSortedSubList(int i, List<Integer> temp) {
        Iterator<Integer> iterator = temp.iterator();
        for (int j = i; j < array.size(); j++) {
            array.set(j, iterator.next());
        }
    }

    /**
     * Retrieves index based on incremented provided integer in provided array.
     */
    private static int getIndexToReset(Integer e, List<Integer> temp) {
        return array.size() - temp.size() + temp.indexOf(e);
    }

    /**
     * Defaults array values (replace values with '1') up to provided index element.
     * For example: [2, 3, 2], 3 -> [1, 1, 2]
     */
    private static void resetArrayUntilE(List<Integer> array, Integer index) {
        for (int j = index - 1; j >= 0 && array.get(j) != 1; j--) {
            array.set(j, 1);
        }
    }

    /**
     * Sorts array in natural integer order
     */
    private static void sortArray(List<Integer> list) {
        list.sort(Integer::compareTo);
    }

    /**
     * Returns calculated product of an array, limiting calculation flow based on provided value
     */
    private static Long calcProductLimited(long limit) {
        long result = 1L;
        for (int i = array.size() - 1; i >= 0 && array.get(i) != 1 && result < limit; i--) {
            result *= array.get(i);
        }

        return result;
    }

    /**
     * Returns Sum of elements in provided List.
     */
    private static Long calcSum(List<Integer> elements) {
        Long res = 0L;
        for (int i = elements.size() - 1; i >= 0; i--) {

            Integer value = elements.get(i);

            if (value == 1) {
                res += i + 1;
                break;
            } else {
                res += value;
            }
        }

        return res;
    }

    /**
     * Returns Array.toString() omitting all repeatable [1, 1, 1, 1, 1...] as a number of such values,
     * in order to truncate output for large arrays.
     */
    private static String omitRepeatableOnes(List<Integer> array) {
        return "[...{" + array.stream().filter(i -> i == 1).count() + "} repeatable ones omitted..., " +
                array.toString().replaceAll("1,", "").replaceAll(" {2}", "").substring(1);
    }
}
