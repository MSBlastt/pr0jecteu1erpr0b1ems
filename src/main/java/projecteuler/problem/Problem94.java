package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.math.BigDecimal;

/**
 * Problem 94: Almost equilateral triangles
 *
 * It is easily proved that no equilateral triangle exists with integral length sides and integral area.
 * However, the almost equilateral triangle 5-5-6 has an area of 12 square units.
 *
 * We shall define an almost equilateral triangle to be a triangle,
 * for which two sides are equal and the third differs by no more than one unit.
 *
 * Find the sum of the perimeters of all almost equilateral triangles,
 * with integral side lengths and area and whose perimeters do not exceed one billion (1,000,000,000).
 * 
 * Created by Mikhail Kholodkov
 *         on July 5, 2017.
 */
public class Problem94 extends ProjectEulerProblem {

    public static void main(String[] args) {
        long perimeterSum = 0L;

        for (int i = 3; i < 333_333_333; i +=2) { //Perimeter cannot be > One Billion
            for (int j = -1, thirdSide = i + j; j <= 1; j += 2) {

                //Perimeter should be an even number, otherwise triangle's area will never be an integral number
                if (!Utils.isEvenBit(i + i + thirdSide)) {
                    continue;
                }

                BigDecimal triangleArea = Utils.getTriangleArea(i, i, thirdSide);

                if (Utils.isIntegerValue(triangleArea)) {
                    System.out.println(
                            String.format("Found integral area: %s, for triangle with sides: %s %s %s",
                            triangleArea.toBigInteger(), i, i, thirdSide));

                    perimeterSum += i + i + thirdSide;

                    i *= 3; //Dirty hack. Empirically I've found that next integral area/side is ~3x of the previous
                }
            }
        }

        printSolution(perimeterSum);
    }
}