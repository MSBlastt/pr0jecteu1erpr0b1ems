package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.Utils;

import java.util.*;

/**
 * Problem 95: Amicable chains
 *
 * The proper divisors of a number are all the divisors excluding the number itself.
 * For example, the proper divisors of 28 are 1, 2, 4, 7, and 14. As the sum of these divisors is equal to 28, we call it a perfect number.
 * 
 * Interestingly the sum of the proper divisors of 220 is 284 and the sum of the proper divisors of 284 is 220, forming a chain of two numbers.
 * For this reason, 220 and 284 are called an amicable pair.
 * 
 * Perhaps less well known are longer chains. For example, starting with 12496, we form a chain of five numbers:
 * 
 * 12496 → 14288 → 15472 → 14536 → 14264 (→ 12496 → ...)
 * 
 * Since this chain returns to its starting point, it is called an amicable chain.
 * 
 * Find the smallest member of the longest amicable chain with no element exceeding one million.
 * 
 * Created by Mikhail Kholodkov
 *         on July 9, 2017.
 */
public class Problem95 extends ProjectEulerProblem {

    public static void main(String[] args) {
        //Pre-calculated set of perfect numbers
        Set<Integer> perfectNumbers = new HashSet<>(Arrays.asList(6, 28, 496, 8128));

        //Collection for all amicable numbers
        Set<Integer> amicableNumbers = new HashSet<>();

        //Map for value -> sum of its divisors, i.e. 9 -> 4, 28 -> 28...
        Map<Integer, Integer> divisorsSumByNumber = new HashMap<>();

        //Populating divisorsSumByNumber map for values below one million
        for (int i = 1; i < 1_000_000; i++) {
            int sum = Utils.getDivisorsSum(i);
            divisorsSumByNumber.put(i, sum);

            Integer value = divisorsSumByNumber.get(sum);
            if (value != null && value == i) { //amicable number found, since its divisors sum and sum's divisors sum equal
                amicableNumbers.add(i);
                amicableNumbers.add(sum);
                print("Found amicable pair: " + i + " --- " + sum);
            }
        }

        lineBreak();

        //Removing perfect numbers below one million, since they would lead to a dead-end during iteration
        perfectNumbers.forEach(amicableNumbers::remove);

        //Helper variables for finding a solution
        int maxChain = 0;
        int maxChainEntryNumber = 0;

        for (Integer i : divisorsSumByNumber.keySet()) {

            if (i % 100_000 == 0) {
                print("Exploring chain for i = " + i); //intermediate output for every 100000th value
            }

            int sum = divisorsSumByNumber.get(i);
            int chainCount = 1;

            /* Iterating over numbers < 1_000_000, building a chain of divisor sums,
               by calculating sum and incrementing counter, unless amicable or perfect number is encountered.
               Once encountered breaking the loop, since these kind of values would stop the chain.
               The longest chain is the key to solution (smallest member).
             */
            while (sum != 1 && sum < 1_000_000 && !perfectNumbers.contains(sum) && !amicableNumbers.contains(sum)) {
                sum = divisorsSumByNumber.get(sum);

                chainCount++;

                if (sum == i) {
                    if (chainCount > maxChain) {
                        maxChain = chainCount;
                        maxChainEntryNumber = i;

                        print("Found max chain of %s numbers, for starting value: %s", maxChain, i);
                        lineBreak();
                    }

                    break;
                }

                //Workaround for very long chains which never link to each other. Perhaps better to be called a dirty hack
                if (chainCount > 5000) {
                    break;
                }
            }
        }

        lineBreak();
        print("Found longest chain of %s elements, starting number is %s", maxChain, maxChainEntryNumber);

        //By knowing entry point of a longest chain, iterating over it one more time and looking for the smallest member.
        int lowestSum = divisorsSumByNumber.get(maxChainEntryNumber);
        int tmp = lowestSum;

        for (int i = 0; i < maxChain; i++) {
            tmp = divisorsSumByNumber.get(tmp);

            if (tmp < lowestSum) {
                lowestSum = tmp;
            }

        }

        printSolution(lowestSum);

    }
}