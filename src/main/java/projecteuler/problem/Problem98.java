package projecteuler.problem;

import projecteuler.ProjectEulerProblem;
import projecteuler.util.FileUtils;
import projecteuler.util.Utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Problem 98: Anagramic squares
 *
 * By replacing each of the letters in the word CARE with 1, 2, 9, and 6 respectively, we form a square number: 1296 = 362.
 * What is remarkable is that, by using the same digital substitutions, the anagram, RACE, also forms a square number: 9216 = 962.
 * We shall call CARE (and RACE) a square anagram word pair and specify further that leading zeroes are not permitted,
 * neither may a different letter have the same digital value as another letter.
 *
 * Using words.txt (right click and 'Save Link/Target As...'),
 * a 16K text file containing nearly two-thousand common English words,
 * find all the square anagram word pairs (a palindromic word is NOT considered to be an anagram of itself).
 *
 * What is the largest square number formed by any member of such a pair?
 *
 * NOTE: All anagrams formed must be contained in the given text file.
 * 
 * Created by Mikhail Kholodkov
 *         on July 15, 2017.
 */
public class Problem98 extends ProjectEulerProblem {

    public static void main(String[] args) {
        long highestSquareNumber = 0; //Solution result

        Map<Integer, Set<Long>> squaresByLength = new HashMap<>(); // Collection of squares by number's length

        //Adding dummy Sets to Map
        for (int i = 1; i < 20; i++) {
            squaresByLength.put(i, new HashSet<>());
        }

        //Getting words dictionary
        String dictionary = FileUtils.getFileContent("p098_words.txt").replaceAll("\"", "");
        List<String> wordsList = Arrays.asList(dictionary.split(","));

        //Looking for longest word, getting its length
        int maxWordLength = wordsList.stream().mapToInt(String::length).max().orElse(0);
        int powLength = 1;

        //Looking for all squares within Long's range, but value's length should not be more than longest word length.
        for (long i = 1; powLength < maxWordLength; i++) {
            long square = (long) Math.pow(i, 2);
            powLength = Long.toString(square).length();

            squaresByLength.get(powLength).add(square);
        }

        System.out.println(String.format("Added %s squares to the map!",
                squaresByLength.values().stream().mapToInt(Set::size).sum()));
        lineBreak();

        //Sorting by words length, slightly increases performance
        wordsList.sort(Comparator.comparingInt(String::length));

        //System.out.println(wordsList); //Optional output

        //Iterating over words by length from 2 to longest
        for (int i = 2; i <= maxWordLength; i++) {
            int finalI = i;

            //Filtering by words length
            List<String> collection = wordsList
                    .stream()
                    .filter(w -> w.length() == finalI)
                    .collect(Collectors.toList());

            //Looking for anagrams within filtered collection of words
            Set<String> anagrams = new HashSet<>();
            for (String s : collection) {
                for (String s1 : collection) {
                    if (!s.equals(s1) && Utils.isPossiblePermutation(s, s1) && !anagrams.contains(s1 + "." + s)) {
                        anagrams.add(s + "." + s1);
                    }
                }
            }

            /* For each found anagramic pair, algorithm iterates over pre-calculated squares
               Each square is getting mapped on the first word from the pair.
               Based on the map, second word is getting transformed is well.
               Is second word is a valid square, then we found a square anagram word pair.
               Looking for the largest square from either side of an anagram.
             */
            for (String anagramPair : anagrams) {

                for (long square : squaresByLength.get(i)) {

                    //Getting both values of an anagram
                    String[] words = anagramPair.split("\\.");
                    String firstWord = words[0];
                    String secondWord = words[1];

                    //Building a map of Character-Integer key-pairs, based on the first word -> square mapping.
                    Map<Character, String> map = new HashMap<>();
                    String squareString = String.valueOf(square);
                    for (int c = 0; c < squareString.length(); c++) {
                        map.put(firstWord.charAt(c), String.valueOf(squareString.charAt(c)));
                    }

                    //As per requirements, a different letter may not have the same digital value as another letter.
                    if (!isAllDigitsUniquePerCharacter(map)) {
                        continue;
                    }

                    String firstWordNumber = "", secondWordNumber = "";

                    //Constructing 2 numbers based on map
                    for (int c = 0; c < squareString.length(); c++) {
                        firstWordNumber += map.get(firstWord.charAt(c));
                        secondWordNumber += map.get(secondWord.charAt(c));
                    }

                    //As per requirements, omitted leading zeroes are not permitted as a valid square value
                    if (!secondWordNumber.startsWith("0") && Utils.isSquare(Long.valueOf(secondWordNumber))) {

                        //Optional console print
                        System.out.println(String.format(
                                "Found an anagramic pair of squares: %s (%s) %s (%s), for square: %s",
                                firstWord, firstWordNumber, secondWord, secondWordNumber, square));

                        //Solution can be either of squares within anagram word pair
                        highestSquareNumber = Math.max(highestSquareNumber, square);
                        highestSquareNumber = Math.max(highestSquareNumber, Long.valueOf(secondWordNumber));
                    }
                }
            }
        }

        printSolution(highestSquareNumber);
    }

    /**
     * Returns true if in provided map all pairs Character-Integer are unique.
     *
     * For example:
     * RACE(1234), CARE(5678) will return true
     * RACE(2131), CARE(5171) will return false, since '1' is a value for both 'A' and 'E'
     */
    private static boolean isAllDigitsUniquePerCharacter(Map<Character, String> map) {
        for (int i = 0; i <= 9; i++) {
            String value = String.valueOf(i);

            if (map.values()
                    .stream()
                    .filter(s -> s.equals(value)).count() > 1) {

                if (map.entrySet()
                        .stream()
                        .filter(entry -> Objects.equals(entry.getValue(), value))
                        .map(Map.Entry::getKey)
                        .count() > 1) {

                    return false;
                }
            }
        }

        return true;
    }
}