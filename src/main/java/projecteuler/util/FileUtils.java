package projecteuler.util;

import projecteuler.problem.Problem98;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Utility class. Contains file manipulation generic methods.
 *
 * Created by Mikhail Kholodkov
 *         on July 15, 2017.
 */
public class FileUtils {

    /**
     * Returns content of the file as a String from resources folder.
     * Filename should contain fully qualified name, including extension.
     *
     * For example: words.txt, project42.txt, etc
     */
    public static String getFileContent(String fileName) {
        StringBuilder result = new StringBuilder();

        //read file into stream, try-with-resources
        try {
            try (Stream<String> stream = Files.lines(Paths.get(Problem98.class
                    .getClassLoader().getResource(fileName).toURI()))) {

                stream.forEach(result::append);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return result.toString();
    }
}
