package projecteuler.util;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Shared Utility class.
 *
 * Contains some common re-usable, usually optimized, methods for basic/advanced math operations.
 *
 * Created by Mikhail Kholodkov
 *         on June 8, 2017.
 */
public class Utils {

    /**
     * Returns a list of primes within a range of integers [from, to]
     *
     * Adds '2' by default.
     *
     * Iterates over odd numbers only. So provided argument 'from' must be an odd number.
     *
     * Overloaded method works for Long range values.
     */
    public static List<Integer> getPrimesArray(int from, int to) {
        return getPrimesArray((long) from, (long) to)
                .stream()
                .mapToInt(Long::intValue)
                .boxed()
                .collect(Collectors.toList());
    }

    public static List<Long> getPrimesArray(long from, long to) {
        return getPrimesArray(from, to, 0);
    }

    public static List<Long> getPrimesArray(long from, long to, int sizeLimit) {
        List<Long> primes = new ArrayList<>();

        if (from <= 2) {
            primes.add(2L);
            from = 3;
        }

        for (long i = from; i < to; i += 2) {
            if (isPrime(i)) {
                primes.add(i);
                if (sizeLimit > 0 && primes.size() == sizeLimit) {
                    return primes;
                }
            }
        }

        System.out.println(String.format("Added %s primes to Collection", primes.size()));

        return primes;
    }

    /**
     * Returns first N primes from the sequence.
     *
     * 2 will return 2, 3
     * 4 will return 2, 3, 5, 7
     * 7 will return 2, 3, 5, 7, 11, 13, 17
     */
    public static List<Long> getFirstNPrimes(int primesCount) {
        return getPrimesArray(1, Long.MAX_VALUE, primesCount);
    }

    /**
     * Returns true if value is a prime number (2, 3, 5, 7, 11, 13, 17...)
     *
     * A prime number (or a prime) is a natural number greater than 1,
     * that has no positive divisors other than 1 and itself.
     *
     * Simplest method which quickly checks if number is more than 2 and even,
     * iterates from 3 to Math.pow(num, 0.5) with an increment of 2.
     */
    public static boolean isPrime(long num) {
        //unique even prime
        if (num == 2) {
            return true;
        }

        //fast even test and incorrect values
        if (num < 3 || (num & 1) == 0) {
            return false;
        }

        // only odd factors need to be tested up to num^0.5
        for (long i = 3; i * i <= num; i += 2)
            if (num % i == 0) {
                return false;
            }

        return true;
    }

    /**
     * Primality test based on internal JDK Miller Rabin implementation.
     *
     * Certainty '1' should be enough to cover almost all false-positive cases within 2^32 Integer range.
     * For hard check isPrime() method can be additionally used.
     *
     * Overloaded method accepts custom certainty value.
     *
     * 3 will return true
     * 21 will return false
     * 23 will return true
     */
    public static boolean isPrimeMillerRabin(long num) {
        return isPrimeMillerRabin(num, 1);
    }

    public static boolean isPrimeMillerRabin(long num, int certainty) {
        return BigInteger.valueOf(num).isProbablePrime(certainty);
    }

    public static boolean isPrimeMillerRabin(int num) {
        return isPrimeMillerRabin((long) num, 1);
    }

    public static boolean isPrimeMillerRabin(int num, int certainty) {
        return isPrimeMillerRabin((long) num, certainty);
    }

    /**
     * RabinMiller primality test for BigInteger object type.
     * Deterministically correct for n < 341 550 071 728 321
     *
     * For other values will use BigInteger.isProbablePrime(10)
     *
     * Implementation takes from here [1]
     *
     * [1] - https://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#Java
     */
    public static boolean isPrimeMillerRabin(BigInteger n) {
        if (n.compareTo(new BigInteger("341550071728321")) >= 0) {
            return n.isProbablePrime(10);
        }

        int intN = n.intValue();
        if (intN == 1 || intN == 4 || intN == 6 || intN == 8) return false;
        if (intN == 2 || intN == 3 || intN == 5 || intN == 7) return true;

        int[] primesToTest = getPrimesToTest(n);
        if (n.equals(new BigInteger("3215031751"))) {
            return false;
        }
        BigInteger d = n.subtract(BigInteger.ONE);
        BigInteger s = BigInteger.ZERO;
        while (d.mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO)) {
            d = d.shiftRight(1);
            s = s.add(BigInteger.ONE);
        }
        for (int a : primesToTest) {
            if (tryComposite(a, d, n, s)) {
                return false;
            }
        }

        return true;
    }

    private static int[] getPrimesToTest(BigInteger n) {
        if (n.compareTo(new BigInteger("3474749660383")) >= 0) {
            return new int[]{2, 3, 5, 7, 11, 13, 17};
        }
        if (n.compareTo(new BigInteger("2152302898747")) >= 0) {
            return new int[]{2, 3, 5, 7, 11, 13};
        }
        if (n.compareTo(new BigInteger("118670087467")) >= 0) {
            return new int[]{2, 3, 5, 7, 11};
        }
        if (n.compareTo(new BigInteger("25326001")) >= 0) {
            return new int[]{2, 3, 5, 7};
        }
        if (n.compareTo(new BigInteger("1373653")) >= 0) {
            return new int[]{2, 3, 5};
        }
        return new int[]{2, 3};
    }

    private static boolean tryComposite(int a, BigInteger d, BigInteger n, BigInteger s) {
        BigInteger aB = BigInteger.valueOf(a);
        if (aB.modPow(d, n).equals(BigInteger.ONE)) {
            return false;
        }
        for (int i = 0; BigInteger.valueOf(i).compareTo(s) < 0; i++) {
            if (aB.modPow(BigInteger.valueOf(2).pow(i).multiply(d), n).equals(n.subtract(BigInteger.ONE))) {
                return false;
            }
        }
        return true;
    }


    /**
     * Swaps two characters in string via regular expression.
     * Works for String and Number type objects.
     *
     * asdf, f, a will return fsda
     * qwerz, z, w will return qzerw
     * 12, 1, 2 will return 21
     */
    public static String swapCharacters(String string, Object firstChar, Object secondChar) {
        return string.replaceFirst(String.format("(%s)(%s)", firstChar, secondChar), "$2$1");
    }


    /**
     * Returns absolute difference between two Integers.
     * Overloaded to support Long numbers as well.
     *
     * 5, 7 will return 2
     * -5, 5 will return 0
     * 10, 1 will return 9
     */
    public static int getDelta(Integer i1, Integer i2) {
        return Math.abs(i1 - i2);
    }

    public static long getDelta(Long i1, Long i2) {
        return Math.abs(i1 - i2);
    }

    /**
     * Returns boolean if two provided strings is a valid permutation of each other.
     *
     * 'abc', 'bca' will return true;
     * 'abc', 'dcd' will return false;
     * '2761', '1267' will return true;
     *
     * Case sensitive.
     * Works for numbers and strings.
     * Uses sorting. Approximate complexity for each string: best-case - O(n), worst-case: O(n log n)
     */
    public static boolean isPossiblePermutation(String str1, String str2) {
        char[] a = str1.toCharArray(), b = str2.toCharArray();

        Arrays.sort(a);
        Arrays.sort(b);

        return Arrays.equals(a, b);
    }

    /**
     * Determines whether provided integer value is a Perfect Square.
     * Works well for Integer range.
     *
     * Has been reworked (implementation taken from here: [1]).
     * Now works 4x-5x times faster rather than plain "Math.sqrt(value) % 1 ==0" check
     *
     * Works 15-20% faster than corresponding method for Long range values due to less heap memory allocation.
     *
     * For implementation details please refer to corresponding SO answer.
     *
     * [1] - https://stackoverflow.com/a/24721612
     */
    public static boolean isSquare(int value) {
        if ((value & 2) == 2 || (value & 7) == 5) {
            return false;
        }

        if ((value & 11) == 8 || (value & 31) == 20) {
            return false;
        }

        if ((value & 47) == 32 || (value & 127) == 80) {
            return false;
        }

        if ((value & 191) == 128 || (value & 511) == 320) {
            return false;
        }

        final int root = (int) Math.sqrt(value);

        return root * root == value;
    }

    /**
     * Optimized version of isSquare() method.
     * Works faster and supports big integer (withing Long) range.
     *
     * I have no idea how it works, implementation copied from here: [1]
     *
     * [1] https://stackoverflow.com/a/18686659
     */
    public static boolean isSquare(long value) {
        // 0xC840C04048404040 computed as: goodMask |= Long.MIN_VALUE >>> (i * i), 0 <= goodMask < 64;
        long goodMask = 0xC840C04048404040L;

        // This tests if the 6 least significant bits are right.
        // Moving the to be tested bit to the highest position saves us masking.
        if (goodMask << value >= 0) {
            return false;
        }

        final int numberOfTrailingZeros = Long.numberOfTrailingZeros(value);
        if ((numberOfTrailingZeros & 1) != 0) { // Each square ends with an even number of zeros.
            return false;
        }

        value >>= numberOfTrailingZeros;

        // Now value is either 0 or odd.
        // In binary each odd square ends with 001.
        // Postpone the sign test until now; handle zero in the branch.
        if ((value & 7) != 1 | value <= 0) return value == 0;

        // Do it in the classical way.
        // The correctness is not trivial as the conversion from long to double is lossy!
        final long tst = (long) Math.sqrt(value);

        return tst * tst == value;
    }

    /**
     * Returns a list of relative primes for N.
     *
     * Two integers a and b are said to be relatively prime or co-prime,
     * if the only positive integer that divides both of them is 1.
     * That is, the only common positive factor of the two numbers is 1.
     *
     * For example: 14 and 15 are relative primes, being commonly divisible by only 1.
     * But 14 and 21 are not, because they are both divisible by 7.
     *
     * Overloaded method accepts a collection of pre-calculated primes,
     * which will skip a call to getPrimesArray(1, n) method.
     */
    public static List<Integer> findRelativePrimes(int n) {
        return findRelativePrimes(n, new HashSet<>(getPrimesArray(1, n)));
    }

    public static List<Integer> findRelativePrimes(int n, Set<Integer> primes) {
        if (primes.contains(n)) {
            return IntStream.range(1, n).boxed().collect(Collectors.toList());
        }

        List<Integer> result = new ArrayList<>();
        result.add(1);

        for (int i = 1; i < n - 1; i += n % 2 == 0 ? 2 : 1) {

            if (n % i != 0) {
                if (isRelativePrime(n, i)) {
                    result.add(i);
                }
            }
        }

        result.add(n - 1);

        return result;
    }

    /**
     * Returns true if 'value' is relatively prime to 'candidate',
     * by calculating theirs greatest common divisor, which should be equal to 1.
     *
     * See @findRelativePrimes
     */
    public static boolean isRelativePrime(int value, int candidate) {
        return getGreatestCommonDivisor(value, candidate) == 1;
    }

    /**
     * Returns greatest common divisor (GCG) for a and b.
     *
     * 7, 3 will return 1
     * 28, 16 will return 4
     * 100, 25 will return 25
     *
     * Fastest found so far, binary algorithm.
     */
    public static int getGreatestCommonDivisor(int a, int b) {
        // Corner cases
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        if (a == 0) return b;
        if (b == 0) return a;
        int a0 = Long.numberOfTrailingZeros(a);
        int b0 = Long.numberOfTrailingZeros(b);
        int t = a0 < b0 ? a0 : b0;
        a >>>= a0;
        b >>>= b0;
        while (a != b) {
            if (a > b) {
                a -= b;
                a >>>= Long.numberOfTrailingZeros(a);
            } else {
                b -= a;
                b >>>= Long.numberOfTrailingZeros(b);
            }
        }

        return a << t;
    }

    /**
     * Returns collection of divisors for an integer, including 1 and value itself.
     *
     * 28 will return 1, 2, 4, 7, 14, 28
     * 7 will return 1, 7 (7 is a prime)
     * 18 will return 1, 2, 3, 6, 9, 18
     *
     * Overloaded method suitable for long range and additional parameter to limit highest divisor.
     */
    public static List<Integer> getDivisors(int n) {
        List<Integer> divisors = new ArrayList<>();

        // Note that this loop runs till square root
        for (int i = 1; i <= Math.sqrt(n) + 1; i++) {
            if (n % i == 0) {
                // If divisors are equal, print only one
                if (n / i == i) {
                    divisors.add(i);
                } else {// Otherwise print both
                    divisors.add(i);
                    divisors.add(n / i);
                }
            }
        }

        return divisors;
    }

    public static List<Long> getDivisors(long value) {
        return getDivisors(value, (value / 2) + 1);
    }

    public static List<Long> getDivisors(long value, long limit) {
        List<Long> result = new ArrayList<>();
        result.add(1L);
        if (limit >= value || limit == (value / 2) + 1) {
            result.add(value);
        }

        for (long i = 2; i <= limit; i++) {
            if (value % i == 0) {
                result.add(i);
            }
        }

        return result;
    }



    /**
     * Count divisors and returns its sum.
     * Works a bit differently comparing to previous methods, for int range works much faster.
     */
    public static Integer getDivisorsSum(Integer input) {
        int maxD = (int) Math.sqrt(input);
        int sum = 1;

        for (int i = 2; i <= maxD; i++) {
            if (input % i == 0) {
                sum += i;
                int d = input / i;
                if (d != i)
                    sum += d;
            }
        }

        return sum;
    }

    /**
     * Returns square root of BigDecimal with long remainder.
     *
     * Works fast with a Scale up to 2^16 (65536).
     *
     * 2, 20 will return 1.41421356237309504880
     */
    public static BigDecimal sqrt(BigDecimal A, final int SCALE) {
        BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
        while (!x0.equals(x1)) {
            x0 = x1;
            x1 = A.divide(x0, SCALE, BigDecimal.ROUND_FLOOR);
            x1 = x1.add(x0);
            x1 = x1.divide(new BigDecimal(2), SCALE, BigDecimal.ROUND_FLOOR);

        }
        return x1;
    }

    /**
     * Returns a square root of given BigInteger.
     * Value must be a valid square, otherwise it'll return rounded integer value
     *
     * BigInteger.valueOf(9) will return 3
     * BigInteger.valueOf(16) will return 4
     */
    public static BigInteger sqrt(BigInteger n) {
        BigInteger a = BigInteger.ONE;
        BigInteger b = new BigInteger(n.shiftRight(5).add(new BigInteger("8")).toString());

        while (b.compareTo(a) >= 0) {
            BigInteger mid = new BigInteger(a.add(b).shiftRight(1).toString());
            if (mid.multiply(mid).compareTo(n) > 0) {
                b = mid.subtract(BigInteger.ONE);
            }
            else {
                a = mid.add(BigInteger.ONE);
            }
        }

        return a.subtract(BigInteger.ONE);
    }

    /**
     * Returns Area of a triangle with sides a, b, c. Uses Heron's formula.
     *
     * Heron's formula states that the area of a triangle whose sides have lengths a, b, and c is:
     * A = sqrt(s*(s-a)*(s-b)*(s-c)), where s is the semi-perimeter of the triangle: s = 1/2*(a+b+c)
     *
     * 5, 5, 6 will return 12
     * 17, 17, 17 will return 120
     * 65, 65, 66 will return 1848
     */
    public static BigDecimal getTriangleArea(int a, int b, int c) {
        int sum = a + b + c;
        double sDouble = sum / 2.0d;

        BigInteger s = BigInteger.valueOf((long) sDouble);

        return  Utils.sqrt(new BigDecimal(
                s.multiply(s.subtract(BigInteger.valueOf(a))
                        .multiply(s.subtract(BigInteger.valueOf(b)))
                        .multiply(s.subtract(BigInteger.valueOf(c))))), 10);
    }

    /**
     * Returns true if number is even (2, 4, 6 , 8...).
     *
     * Uses bit shift, slightly faster than mod division.
     */
    public static boolean isEvenBit(int i) {
        return (i & 1) == 0;
    }

    /**
     * Returns true if number is even (2, 4, 6 , 8...).
     *
     * Uses mod division, slightly slower than bit shift.
     */
    public static boolean isEvenMod(int i) {
        return i % 2 == 0;
    }

    /**
     * Returns true if provided BigDecimal object has an integer value.
     *
     * BigDecimal.valueOf(2.5d) will return false
     * BigDecimal.valueOf(2.0d) will return true
     * BigDecimal.valueOf(3L) will return true
     */
    public static boolean isIntegerValue(BigDecimal bd) {
        return bd.signum() == 0 || bd.scale() <= 0 || bd.stripTrailingZeros().scale() <= 0;
    }

    /**
     * Returns true if provided double value is integer.
     *
     * 3.0 will return true
     * 3.5 will return false
     * 4.01 will return false
     */
    public static boolean isInteger(double value) {
        return value == (int) value;
    }

    /**
     * Returns true if provided number is (1-9) pandigital.
     *
     * Pandigital number is an integer that has among its digits each digit used at least once,
     * order irrespectively.
     *
     * For example:
     *
     * 123456789 will return true
     * 123456999 will return false
     * 112233445 will return false
     */
    public static boolean isPandigital(int number) {
        int digits = 0;
        int count = 0;

        for (; number > 0; number /= 10, ++count) {
            if (digits == (digits |= 1 << (number - ((number / 10) * 10) - 1)))
                return false;
        }

        return digits == (1 << count) - 1;
    }

    /**
     * Checks if provided int is a palindrome ('585', going reverse is the same number '585')
     *
     * For String input:
     * Recursively comparing characters, [first, last]; [first + 1, last - 1]; [first + 2, last - 2]...
     *
     * For Integer input:
     * Number is getting reversed by getting remainder for each division on 10 and summarizing it to temp variable
     *
     * Integer input works for both positive and negative integers.
     *
     * Overloaded methods accept Long range integer values or directly String.
     *
     * 585 will return true
     * 122 will return false
     * 7997 will return true
     */
    public static boolean isPalindrome(int number) {
        return isPalindrome((long) number);
    }

    public static boolean isPalindrome(long number) {
        long palindrome = number; // copied number into variable
        long reverse = 0;

        while (palindrome != 0) {
            long remainder = palindrome % 10;
            reverse = reverse * 10 + remainder;
            palindrome = palindrome / 10;
        }

        //If original and reverse of number is equal means number is palindrome
        return number == reverse;
    }

    public static boolean isPalindrome(String str) {
        //Test end of recursion
        if (str.length() < 2) {
            return true;
        }

        //Checks first and last character for equality
        if (str.charAt(0) != str.charAt(str.length() - 1)) {
            return false;
        }

        //Recursion call
        return isPalindrome(str.substring(1, str.length() - 1));
    }

    /**
     * Returns true if provided value is a cube.
     *
     * 9 will return false
     * 27 will return true
     * 125 will return true
     */
    private static boolean isCube(double value) {
        double wannabeCube = Math.cbrt(value);
        return Math.round(wannabeCube) == wannabeCube;
    }


    /*
        NOT YET FINISHED/TESTED METHODS BELOW
     */

    public static String removeLeadingZeroes(String string) {
        return string.replaceFirst("^0+(?!$)", "");
    }

    public static int getArraySum(List<Integer> numbers) {
        return numbers.stream().mapToInt(Integer::intValue).sum();
    }
}